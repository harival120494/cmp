import Page from 'components/Page';
import React from 'react';
import {
  Button,
  Card,
  CardBody,
  Col,
  Row,
} from 'reactstrap';

function Package() {
  return (
    <Page title="Package" breadcrumbs={[{ name: 'Package', active: true }]}>

      <Row className="mt-4">
        <Col>
          <Card>
            <CardBody>
              <Row>              
                <Col md={4} sm={12} className="p-2">
                  <h3>Nama Paket</h3>
                  <h5>Rp. 100.000 / Bulan</h5>
                </Col>           
                <Col md={2} sm={6} className="p-2 d-flex align-items-center justify-content-center">
                  Maks. Toko 
                </Col>            
                <Col md={3} sm={6} className="p-2 d-flex align-items-center justify-content-center">
                  1 Toko
                </Col>           
                <Col md={3} sm={12} className="p-2 d-flex align-items-center justify-content-center">
                  <Row>
                    <Col>
                      <Button outline color="primary" block>Detail</Button>
                    </Col>
                    <Col>
                      <Button color="primary" block>Pilih</Button>
                    </Col>
                  </Row>
                </Col>
              </Row>
            </CardBody>
          </Card>
        </Col>
      </Row>

    </Page>
  );
};

export default Package;
