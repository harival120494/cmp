import Page from 'components/Page';
import React from 'react';
import { API_URL } from '../../utils/constants';
import {
  Card,
  CardBody,
  CardImg,
  Col,
  Row
} from 'reactstrap';

function Marketplace() {
  console.log(API_URL);

  return (
    <Page title="Marketplace" breadcrumbs={[{ name: 'Marketplace', active: true }]}>

      <Row className="mt-4">

        <Col md={3} sm={6}>
          <Card>
            <CardImg variant="top" className="img-fluid" src="https://media.istockphoto.com/photos/business-sign-that-says-u2018openu2019-on-cafe-or-restaurant-hang-on-picture-id1298039173?b=1&k=20&m=1298039173&s=170667a&w=0&h=KU0J6vElLr49nfgOFPem3H3lkqmtqn9mzGbCseLnmXw=" />
            <CardBody className="text-center">
              <h5>Shopee</h5>
            </CardBody>
          </Card>
        </Col>

        <Col md={3} sm={6}>
          <Card>
            <CardImg variant="top" className="img-fluid" src="https://images.unsplash.com/photo-1472851294608-062f824d29cc?ixlib=rb-1.2.1&ixid=MnwxMjA3fDB8MHxzZWFyY2h8Mnx8c3RvcmV8ZW58MHx8MHx8&auto=format&fit=crop&w=600&q=60" />
            <CardBody className="text-center">
              <h5>Tokopedia</h5>
            </CardBody>
          </Card>
        </Col>

        <Col md={3} sm={6}>
          <Card>
            <CardImg variant="top" className="img-fluid" src="https://media.istockphoto.com/photos/business-sign-that-says-u2018openu2019-on-cafe-or-restaurant-hang-on-picture-id1298039173?b=1&k=20&m=1298039173&s=170667a&w=0&h=KU0J6vElLr49nfgOFPem3H3lkqmtqn9mzGbCseLnmXw=" />
            <CardBody className="text-center">
              <h5>Lazada</h5>
            </CardBody>
          </Card>
        </Col>

        <Col md={3} sm={6}>
          <Card>
            <CardImg variant="top" className="img-fluid" src="https://images.unsplash.com/photo-1472851294608-062f824d29cc?ixlib=rb-1.2.1&ixid=MnwxMjA3fDB8MHxzZWFyY2h8Mnx8c3RvcmV8ZW58MHx8MHx8&auto=format&fit=crop&w=600&q=60" />
            <CardBody className="text-center">
              <h5>Bukalapak</h5>
            </CardBody>
          </Card>
        </Col>

      </Row>

    </Page>
  );
};

export default Marketplace;
