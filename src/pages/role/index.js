import React from 'react'
import { useSelector } from 'react-redux';
import AdminRole from '../../components/Role/AdminRole';


function Role() {

  const {role_id} = useSelector((state) => state.LoginReducer);
  let RenderComponent;

  if (role_id === 1) {
    RenderComponent = <AdminRole />;
  } else {
    RenderComponent = <h2>Access Denied</h2>;
  }

  return (
    <>
      { RenderComponent }
    </>
  );
};

export default Role;
