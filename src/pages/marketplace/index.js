import React from 'react'
import { useSelector } from 'react-redux';
import UserMarketplace from '../../components/Marketplace/UserMarketplace';
import AdminMarketplace from '../../components/Marketplace/AdminMarketplace';


function Marketplace() {

  const {role_id} = useSelector((state) => state.LoginReducer);
  let RenderComponent;

  if (role_id === 1) {
    RenderComponent = <AdminMarketplace />;
  } else if (role_id === 2) {
    RenderComponent = <UserMarketplace />;
  } else {
    RenderComponent = <h2>Denied</h2>;
  }

  return (
    <>
      { RenderComponent }
    </>
  );
};

export default Marketplace;
