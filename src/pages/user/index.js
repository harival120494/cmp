import React from 'react'
import { useSelector } from 'react-redux';
import AdminUser from '../../components/User/AdminUser';


function User() {

  const {role_id} = useSelector((state) => state.LoginReducer);
  let RenderComponent;

  if (role_id === 1) {
    RenderComponent = <AdminUser />;
  } else {
    RenderComponent = <h2>Access Denied</h2>;
  }

  return (
    <>
      { RenderComponent }
    </>
  );
};

export default User;
