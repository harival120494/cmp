import React from 'react'
import { useSelector } from 'react-redux';
import AdminPeriode from '../../components/Periode/AdminPeriode';


function Periode() {

  const {role_id} = useSelector((state) => state.LoginReducer);
  let RenderComponent;

  if (role_id === 1) {
    RenderComponent = <AdminPeriode />;
  } else {
    RenderComponent = <h2>Access Denied</h2>;
  }

  return (
    <>
      { RenderComponent }
    </>
  );
};

export default Periode;
