import React from 'react'
import { useSelector } from 'react-redux';
import AdminBank from '../../components/Bank/AdminBank';


function Bank() {

  const {role_id} = useSelector((state) => state.LoginReducer);
  let RenderComponent;

  if (role_id === 1) {
    RenderComponent = <AdminBank />;
  } else {
    RenderComponent = <h2>Access Denied</h2>;
  }

  return (
    <>
      { RenderComponent }
    </>
  );
};

export default Bank;
