import Page from 'components/Page';
import React, {useEffect} from 'react';
import {useSelector} from 'react-redux';

function DashboardPage(){
  const {username, password} = useSelector((state) => state.LoginReducer);
  useEffect(() => {
    console.log(`${username} == ${password}`);
  },[])
  return (
          <Page
            className="DashboardPage"
            title="Dashboard"
            breadcrumbs={[{ name: 'Dashboard', active: true }]}
          >
            <h5>Dashboard yes !</h5>
          </Page>
        );
}
export default DashboardPage;
