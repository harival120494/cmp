import AuthForm, { STATE_LOGIN } from 'components/AuthForm';
import React, { useCallback } from 'react';
import { Card, Col, Row } from 'reactstrap';

function AuthPage(props) {

  const handleAuthState = useCallback(authState => {
    if (authState === STATE_LOGIN) {
      props.history.push('/login');
    } else {
      props.history.push('/signup');
    }
  });

    return (
      <Row
        style={{
          height: '100vh',
          justifyContent: 'center',
          padding: '50px',
        }}>
        <Col md={6} lg={5} className="p-2">
          <Card body className="text-center shadow border-0">
            <div className='p-4'>
              <h3>Connector Marketplace<br />Solution For Your Marketplace</h3>
            </div>
          </Card>
        </Col>
        <Col md={6} lg={5} className="p-2">
          <Card body className="shadow border-0">
            <AuthForm
              authState={props.authState}
              onChangeAuthState={handleAuthState}
            />
          </Card>
        </Col>
      </Row>
    );
    
}

export default AuthPage;
