import React from 'react';
import { useSelector } from 'react-redux';
import UserPackage from '../../components/Package/UserPackage';
import AdminPackage from '../../components/Package/AdminPackage';

function Package() {

  const {role_id} = useSelector((state) => state.LoginReducer);
  let RenderComponent;

  if (role_id === 1) {
    RenderComponent = <AdminPackage />;
  } else if (role_id === 2) {
    RenderComponent = <UserPackage />;
  } else {
    RenderComponent = <h2>Access Denied</h2>;
  }

  return (
    <>
      { RenderComponent }
    </>
  );

};

export default Package;