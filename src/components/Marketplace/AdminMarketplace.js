import Page from 'components/Page';
import React, { useEffect, useState } from 'react';
import { API_URL } from '../../utils/constants';
import axios from 'axios';
import Swal from 'sweetalert2';
import { useSelector } from 'react-redux';
import ModalAddMarketplace from './ModalAddMarketplace';
import ModalDetailMarketplace from './ModalDetailMarketplace';
import {
  Col,
  Row,
  Table,
  Card,
  Button,
  Breadcrumb
} from 'react-bootstrap';
import { 
  MdList,
  MdEdit,
  MdAddCircleOutline
} from 'react-icons/md';


function AdminMarketplace() {

  const [marketplaceData, setMarketplaceData] = useState([]);
  const [marketplaceDetail, setMarketplaceDetail] = useState({});
  const [marketplaceEdit, setMarketplaceEdit] = useState({});
  const [isUpdate, setIsUpdate] = useState(false);

  const {token} = useSelector((state) => state.LoginReducer);
  const [modalAddShow, setModalAddShow] = useState(false);
  const [modalDetailShow, setModalDetailShow] = useState(false);
  const [mount, setMount] = useState(false);


  useEffect(() => {
    getData();  
  }, []);

  const getData = () => {
    const header = {
      headers : {
        authorization: token
      }
    }
    axios.get(API_URL + 'api/v1/marketplace', header)
    .then((response) => {   
      setMarketplaceData(response.data.data);     
      setMount(true);
    })
    .catch(function (error) {
      Swal.fire({
        icon: 'error',
        title: 'Something Went Wrong',
        text: error,
      });
    })
  }

  const handlerAdd = () => {
    setModalAddShow(true);
  }

  const handlerDetail = (dataDetail) => {
    setModalDetailShow(true);
    setMarketplaceDetail(dataDetail);
  }

  const handlerEdit = (dataEdit) => {
    setModalAddShow(true);
    setMarketplaceEdit(dataEdit);
    setIsUpdate(true);
  }

  return (
    <Page title="Daftar Marketplace" breadcrumbs={[{ name: 'List Marketplace', active: true }]}>
    <Breadcrumb>
      <Breadcrumb.Item href="#">Home</Breadcrumb.Item>
      <Breadcrumb.Item active>Marketplace</Breadcrumb.Item>
    </Breadcrumb>
    <Card className="p-2 border-0" style={{ borderRadius : '1rem', boxShadow: '0 0.5rem 1rem rgb(0 0 0 / 5%)' }}>
      <Card.Body>
        <Row>
          <Col className="text-right">
                <Button variant="outline-primary" onClick={() => handlerAdd()}>
                  <MdAddCircleOutline /> Tambah
                </Button>
          </Col>
        </Row>
        <Row>
        <Col>

        <Table hover className="text-center" style={{ fontSize: 12 }}>
          <thead>
            <tr>
              <th>No</th>
              <th>Kode Marketplace</th>
              <th>Nama Marketplace</th>
              <th>Deskripsi</th>
              <th>Status</th>
              <th>Action</th>
            </tr>
          </thead>
          <tbody>
            {
              mount  ? (
                marketplaceData.map((val, key) => {
                  return(<tr key={"marketplace_id" + key}>
                    <td className="align-middle">{key += 1}</td>
                    <td className="align-middle">{val.marketplace_code}</td>
                    <td className="align-middle">{val.marketplace_name}</td>
                    <td className="align-middle">{val.desc}</td>
                    <td className="align-middle">{val.status === 1 ? "Aktif" : "Nonaktif" }</td>
                    <td className="align-middle">
                      <Button size="sm" variant="warning" className="mr-1" onClick={() => handlerEdit(val)}>
                        <MdEdit />
                      </Button>
                      <Button size="sm" variant="success" onClick={() => handlerDetail(val)}>
                        <MdList />
                      </Button>
                    </td>
                  </tr>)
                })        
              ) : (
                <tr>
                   <td className='align-middle' colSpan={7}>Loading..</td>
                </tr>
              )
            }
                         
          </tbody>
        </Table>

        <ModalAddMarketplace
            isupdate = {isUpdate.toString()}
            show = {modalAddShow}
            onHide = {() => {
              setModalAddShow(false);
              setIsUpdate(false);
              getData();
              setMarketplaceEdit('');
            }}
            token = {token}
            data = {marketplaceEdit}
          />

        <ModalDetailMarketplace
          show={modalDetailShow}
          onHide = {() => {
            setModalDetailShow(false);
            getData();
          }}
          token={token}
          data={marketplaceDetail}
        />

        </Col>
        </Row>   
      </Card.Body>
    </Card>
   

    </Page>
  );
};

export default AdminMarketplace;
