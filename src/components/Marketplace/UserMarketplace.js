import Page from 'components/Page';
import React, { useEffect, useState } from 'react';
import { API_URL, nf } from '../../utils/constants';
import axios from 'axios';
import Swal from 'sweetalert2';
import { useSelector } from 'react-redux';
import {
  Card,
  CardBody,
  Col,
  Row,
  Button
} from 'reactstrap';


function UserPackage() {

  const [marketplaceData, setMarketplaceData] = useState([]);
  const {token} = useSelector((state) => state.LoginReducer);

  useEffect(() => {

    const header = {
      headers : {
        authorization: token
      }
    }
    axios.get(API_URL + 'api/v1/package', header)
    .then((response) => {
      
      setMarketplaceData( response.data.data);
     
    })
    .catch(function (error) {
      Swal.fire({
        icon: 'error',
        title: 'Something Went Wrong',
        text: error,
      });
    })
    
  }, []);


  return (
    <Page title="Marketplace" breadcrumbs={[{ name: 'Marketplace', active: true }]}>

    <Card className="p-2 border-0" style={{ borderRadius : '1rem', boxShadow: '0 0.5rem 1rem rgb(0 0 0 / 5%)' }}>
      <CardBody>
        <Row>
          <Col>

            {marketplaceData.map((val, key) => {
              return (<Card className="mt-2" key={"marketplace_id" + key}>
              <CardBody>

                <Row key={key}>              
                    <Col md={4} sm={12} className="p-2">
                      <h3>{val.package_name}</h3>
                      <h5>Rp {nf.format(val.price)} / Bulan</h5>
                    </Col>           
                    <Col md={2} sm={6} className="p-2 d-flex align-items-center justify-content-center">
                      Maks. Toko 
                    </Col>            
                    <Col md={3} sm={6} className="p-2 d-flex align-items-center justify-content-center">
                      1 Toko
                    </Col>           
                    <Col md={3} sm={12} className="p-2 d-flex align-items-center justify-content-center">
                      <Row>
                        <Col>
                          <Button outline color="primary" block>Detail</Button>
                        </Col>
                        <Col>
                          <Button color="primary" block>Pilih</Button>
                        </Col>
                      </Row>
                    </Col>
                </Row>

              </CardBody>
            </Card>)                
            })}

          </Col>
        </Row>
      </CardBody>
    </Card>
      

    </Page>
     
  );
};

export default UserPackage;
