import Page from 'components/Page';
import React, { useEffect, useState } from 'react';
import { API_URL } from '../../utils/constants';
import axios from 'axios';
import Swal from 'sweetalert2';
import { useSelector } from 'react-redux';
import ModalAddBank from './ModalAddBank';
import ModalDetailBank from './ModalDetailBank';
import {
  Col,
  Row,
  Table,
  Card,
  Button
} from 'react-bootstrap';
import { 
  MdList,
  MdEdit,
  MdAddCircleOutline
} from 'react-icons/md';


function AdminBank() {

  const [bankData, setBankData] = useState([]);
  const [bankDetail, setBankDetail] = useState({});
  const [bankEdit, setBankEdit] = useState({});

  const {token} = useSelector((state) => state.LoginReducer);
  const [modalAddShow, setModalAddShow] = useState(false);
  const [modalDetailShow, setModalDetailShow] = useState(false);
  const [isUpdate, setIsUpdate] = useState(false);
  const [mount, setMount] = useState(false);


  useEffect(() => {
    getData();  
  }, []);

  const getData = () => {
    const header = {
      headers : {
        authorization: token
      }
    }
    axios.get(API_URL + 'api/v1/bank', header)
    .then((response) => {   
      setBankData(response.data.data);    
      setMount(true);    
    })
    .catch(function (error) {
      Swal.fire({
        icon: 'error',
        title: 'Something Went Wrong',
        text: error,
      });
    })
  }

  const handlerAdd = () => {
    setModalAddShow(true);
  }

  const handlerDetail = (dataDetail) => {
    setModalDetailShow(true);
    setBankDetail(dataDetail);
  }

  const handlerEdit = (dataEdit) => {
    setModalAddShow(true);
    setBankEdit(dataEdit);
    setIsUpdate(true);
  }

  return (
    <Page title="Daftar Bank" breadcrumbs={[{ name: 'List Bank', active: true }]}>
      
    <Card className="p-2 border-0" style={{ borderRadius : '1rem', boxShadow: '0 0.5rem 1rem rgb(0 0 0 / 5%)' }}>
      <Card.Body>
        <Row>
          <Col className="text-right">
                <Button variant="outline-primary" onClick={() => handlerAdd()}>
                  <MdAddCircleOutline /> Tambah
                </Button>
          </Col>
        </Row>
        <Row>
        <Col>

        <Table className="text-center" style={{ fontSize: 12 }}>
          <thead>
            <tr>
              <th>No</th>
              <th>Kode Bank</th>
              <th>Nama Bank</th>
              <th>Nomor Rekening</th>
              <th>Nama Pemilik</th>
              <th>Status</th>
              <th>Action</th>
            </tr>
          </thead>
          <tbody>
            {
              mount ? (
                bankData.map((val, key) => {
                  return(<tr key={"bank_id" + key}>
                    <td>{key += 1}</td>
                    <td>{val.bank_code}</td>
                    <td>{val.bank_name}</td>
                    <td>{val.bank_account_number}</td>
                    <td>{val.bank_account_name}</td>
                    <td>{val.status === 1 ? "Aktif" : "Nonaktif" }</td>
                    <td>
                      <Button size="sm" variant="warning" className="mr-1" onClick={() => handlerEdit(val)}>
                        <MdEdit />
                      </Button>
                      <Button size="sm" variant="success" onClick={() => handlerDetail(val)}>
                        <MdList />
                      </Button>
                    </td>
                  </tr>)
                })
              ) : (
                <tr>
                   <td className='align-middle' colSpan={7}>Loading..</td>
                </tr>
              )              
            }
                         
          </tbody>
        </Table>


        <ModalAddBank
            isupdate = {isUpdate.toString()}
            show = {modalAddShow}
            onHide = {() => {
              setModalAddShow(false);
              setIsUpdate(false);
              getData();
              setBankEdit('');
            }}
            token = {token}
            data = {bankEdit}
          />

        <ModalDetailBank
          show={modalDetailShow}
          onHide = {() => {
            setModalDetailShow(false);
            getData();
          }}
          token={token}
          data={bankDetail}
        />

        </Col>
        </Row>   
      </Card.Body>
    </Card>
   

    </Page>
  );
};

export default AdminBank;
