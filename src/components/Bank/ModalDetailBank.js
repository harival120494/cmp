import React from "react";
import Swal from "sweetalert2";
import { API_URL } from "../../utils/constants";
import axios from "axios";
import {
    Modal,
    Button
} from 'react-bootstrap';
import { 
    MdToggleOff,
    MdToggleOn
  } from 'react-icons/md';

function ModalDetailBank(props) {

    const token = props.token;
    const data = props.data;
    const id = props.data.bank_id;
    const name = props.data.bank_name;

    let status;
    if(data.status === 1) {
        status = "Aktif";
    } else {
        status = "Nonaktif";
    }

    let renderButton;
    if (data.status === 1) {
        renderButton = <Button size="sm" variant="outline-dark" onClick={() => inActivateData()}>
                            <MdToggleOff />
                        </Button>;
      } else {
        renderButton = <Button size="sm" variant="outline-dark" onClick={() => activateData()}>
                            <MdToggleOn />
                        </Button>;
      }

    const activateData = () => {
        Swal.fire({
            title: 'Konfirmasi',
            text: "Apakah Anda Yakin Akan Mengaktifkan " + name + "?",
            showCancelButton: true,
            confirmButtonText: 'Aktifkan',
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            cancelButtonText: 'Batal',
            reverseButtons: true
        }).then((result) => {
            if (result.value) {
            const header = {
                headers : {
                authorization: token
                }
            }
            const data = {
                bank_id: id,
                status: 1
            }
            axios.post(API_URL + 'api/v1/bank/change-status', data, header)
            .then(() => {      
                Swal.fire({
                icon: 'success',
                title: name + ' Berhasil Diaktifkan',
                showConfirmButton: false,
                timer: 1000
                }) 
                .then( () => {
                    props.onHide();
                }); 
            })
            .catch(function (error) {
                Swal.fire({
                icon: 'error',
                title: 'Something Went Wrong',
                text: error,
                });
            })
            }
        })
    }
    const inActivateData = () => {
        Swal.fire({
            title: 'Konfirmasi',
            text: "Apakah Anda Yakin Akan Menonaktifkan " + name + "?",
            showCancelButton: true,
            confirmButtonText: 'Nonaktifkan',
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            cancelButtonText: 'Batal',
            reverseButtons: true
        }).then((result) => {
            if (result.value) {
            const header = {
                headers : {
                authorization: token
                }
            }
            const data = {
                bank_id: id,
                status: 0
            }
            axios.post(API_URL + 'api/v1/bank/change-status', data, header)
            .then(() => {      
                Swal.fire({
                icon: 'success',
                title: name + ' Berhasil Dinonaktifkan',
                showConfirmButton: false,
                timer: 1000
                })  
                .then( () => {
                    props.onHide();
                }); 
            })
            .catch(function (error) {
                Swal.fire({
                icon: 'error',
                title: 'Something Went Wrong',
                text: error,
                });
            })
            }
        })
    }

    
    return (
        <Modal
            {...props}
            size="lg"
            aria-labelledby="contained-modal-title-vcenter"
            centered
        >           
            <Modal.Header closeButton>
                <Modal.Title id="contained-modal-title-vcenter">
                    Detail Bank
                </Modal.Title>
            </Modal.Header>
            <Modal.Body>

                <div className="px-4">

                    <div className="row py-3">
                        <div className="col-md-4 font-weight-bold">
                            Kode Bank
                        </div>
                        <div className="col-md-8">
                            {data.bank_code}
                        </div>
                    </div>

                    <div className="row py-3">
                        <div className="col-md-4 font-weight-bold">
                            Nama Bank
                        </div>
                        <div className="col-md-8">
                            {data.bank_name}
                        </div>
                    </div>
                    
                    <div className="row py-3">
                        <div className="col-md-4 font-weight-bold">
                            Nomor Rekening
                        </div>
                        <div className="col-md-8">
                            {data.bank_account_number}
                        </div>
                    </div>
                    
                    <div className="row py-3">
                        <div className="col-md-4 font-weight-bold">
                            Nama Pemilik
                        </div>
                        <div className="col-md-8">
                            {data.bank_account_name}
                        </div>
                    </div>
                    
                    <div className="row py-3">
                        <div className="col-md-4 font-weight-bold">
                            Status
                        </div>
                        <div className="col-md-8">
                            { renderButton }  {status}
                        </div>
                    </div>

                </div>
                           
            </Modal.Body>
            <Modal.Footer>
                <Button variant="outline-danger" onClick={props.onHide}>Kembali</Button>          
            </Modal.Footer>
        </Modal>
    );
}

export default ModalDetailBank;