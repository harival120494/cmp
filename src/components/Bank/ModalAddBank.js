import React, {useState} from "react";
import Swal from "sweetalert2";
import { API_URL } from "../../utils/constants";
import axios from "axios";
import {
    Modal,
    Button
} from 'react-bootstrap';

function ModalAddBank(props) {

    const data = props.data;
    const id = data.bank_id;

    const [kode, setKode] = useState('');
    const [nama, setNama] = useState('');
    const [norek, setNorek] = useState('');
    const [pemilik, setPemilik] = useState('');

    let modalTitle;
    if(props.isupdate === "true") {
        modalTitle = "Ubah Bank";
    } else {
        modalTitle = "Tambah Bank";
    }

    const handleSubmit = async (e) => {
        
        if (kode === '') {
            ErrorAlert("Kode Bank Harus Diisi");
            return false;
        }
        if (nama === '') {
            ErrorAlert("Nama Bank Harus Diisi");
            return false;
        }
        if (norek === '') {
            ErrorAlert("Nomor Rekening Harus Diisi");
            return false;
        }
        if (pemilik === '') {
            ErrorAlert("Nama Pemilik Harus Diisi");
            return false;
        }

        const data = {
            bank_code: kode,
            bank_name: nama,
            bank_account_number: norek,
            bank_account_name: pemilik,
            status: 1
        }
        
        const header = {
            headers : {
                authorization: props.token
            }
        }
        await axios.post(API_URL + 'api/v1/bank', data, header)
        .then((response) => {
            if(response.data.status === 201) {
            Swal.fire({
                icon: 'success',
                title: 'Data Berhasil Disimpan',
                showConfirmButton: false,
                timer: 1000
            })
            .then( () => {
                props.onHide();
                setEmptyForm();
            });          
            } else if (response.data.status === 204) {
                Swal.fire({
                    icon: 'error',
                    title: 'Error',
                    text: response.data.message
                })
                .then( () => {
                    setEmptyForm();
                }); 
            }
            
        })
        .catch(function (error) {
            Swal.fire({
                icon: 'error',
                title: 'Something Went Wrong',
                text: error
            })
            .then( () => {
                setEmptyForm();
            }); 
        });
    };

    const handleUpdate = async (e) => {
        let dataKode;
        let dataNama;
        let dataNorek;
        let dataPemilik;

        if (kode) {
            dataKode = kode;
        } else {
            dataKode = props.data.bank_code;
        }

        if (nama) {
            dataNama = nama;
        } else {
            dataNama = props.data.bank_name;
        }

        if (norek) {
            dataNorek = norek;
        } else {
            dataNorek = props.data.bank_account_number;
        }

        if (pemilik) {
            dataPemilik = pemilik;
        } else {
            dataPemilik = props.data.bank_account_name;
        }

        
        if (dataKode === '') {
            ErrorAlert("Deskripsi Harus Diisi");
            return false;
        }
        if (dataNama === '') {
            ErrorAlert("Nama Bank Harus Diisi");
            return false;
        }
        if (dataNorek === '') {
            ErrorAlert("Nomor Rekening Harus Diisi");
            return false;
        }
        if (dataPemilik === '') {
            ErrorAlert("Nama Pemilik Harus Diisi");
            return false;
        }

        const data = {
            bank_code: dataKode,
            bank_name: dataNama,
            bank_account_number: dataNorek,
            bank_account_name: dataPemilik,
            status: 1
        }
        
        const header = {
            headers : {
                authorization: props.token
            }
        }
        await axios.put(API_URL + 'api/v1/bank/' + id, data, header)
        .then((response) => {
            if(response.data.status === 200) {
            Swal.fire({
                icon: 'success',
                title: 'Data Berhasil Diperbarui',
                showConfirmButton: false,
                timer: 1000
            })
            .then( () => {
                props.onHide();
                setEmptyForm();
            });          
            } else if (response.data.status === 204) {
                Swal.fire({
                    icon: 'error',
                    title: 'Error',
                    text: response.data.message
                })
                .then( () => {
                    setEmptyForm();
                }); 
            }
            
        })
        .catch(function (error) {
            Swal.fire({
                icon: 'error',
                title: 'Something Went Wrong',
                text: error
            })
            .then( () => {
                setEmptyForm();
            });
        });
    };

    const setEmptyForm = () => {
        setKode('');
        setNama('');
        setNorek('');
        setPemilik('');
    }

    const ErrorAlert = (string) => {
        Swal.fire({
            icon: 'error',
            title: 'Oops',
            text: string,
        });
    };
    
   
    return (
        <Modal
            {...props}
            size="lg"
            aria-labelledby="contained-modal-title-vcenter"
            centered
        >           
            <Modal.Header closeButton>
                <Modal.Title id="contained-modal-title-vcenter">
                    { modalTitle }
                </Modal.Title>
            </Modal.Header>
            <Modal.Body>
                <form className="px-4">                      

                    <div className="form-group row">
                        <label className="col-md-3 col-form-label">Kode Bank</label>
                        <div className="col-md-9">
                            <input type="number" className="form-control" placeholder="Kode Bank" onChange={(e) => setKode(e.target.value)} defaultValue={ props.isupdate === "true" ? data.bank_code : '' } />
                        </div>
                    </div>                

                    <div className="form-group row">
                        <label className="col-md-3 col-form-label">Nama Bank</label>
                        <div className="col-md-9">
                            <input type="text" className="form-control" placeholder="Nama Role" onChange={(e) => setNama(e.target.value)} defaultValue={ props.isupdate === "true" ? data.bank_name : '' } />
                        </div>
                    </div>               

                    <div className="form-group row">
                        <label className="col-md-3 col-form-label">Nomor Rekening</label>
                        <div className="col-md-9">
                            <input type="number" className="form-control" placeholder="Nomor Rekening" onChange={(e) => setNorek(e.target.value)} defaultValue={ props.isupdate === "true" ? data.bank_account_number : '' } />
                        </div>
                    </div>               

                    <div className="form-group row">
                        <label className="col-md-3 col-form-label">Nama Pemilik</label>
                        <div className="col-md-9">
                            <input type="text" className="form-control" placeholder="Nama Pemilik" onChange={(e) => setPemilik(e.target.value)} defaultValue={ props.isupdate === "true" ? data.bank_account_name : '' } />
                        </div>
                    </div>    
                                        
                </form>
               
            </Modal.Body>
            <Modal.Footer>
                <Button variant="outline-danger" onClick={props.onHide}>Batal</Button>
                <Button variant="primary mr-4" onClick={ props.isupdate === "true" ? handleUpdate : handleSubmit }>Simpan</Button>            
            </Modal.Footer>
        </Modal>
    );
}

export default ModalAddBank;