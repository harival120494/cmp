import Page from 'components/Page';
import React, { useEffect, useState } from 'react';
import { API_URL, nf } from '../../utils/constants';
import axios from 'axios';
import Swal from 'sweetalert2';
import { useSelector } from 'react-redux';
import {
  Col,
  Row,
  Table,
  Button,
  Card
} from 'react-bootstrap';
import ModalAddPackage from './ModalAddPackage';
import ModalDetailPackage from './ModalDetailPackage';
import { 
  MdEdit,
  MdAddCircleOutline,
  MdList
} from 'react-icons/md';


function AdminPackage() {

  const [packageData, setPackageData] = useState([]);
  const [packageDetail, setPackageDetail] = useState({});
  const [packageEdit, setPackageEdit] = useState({});

  const [periodData, setPeriodData] = useState([]);
  const [marketplaceData, setMarketplaceData] = useState([]);
  const [isUpdate, setIsUpdate] = useState(false);

  const {token} = useSelector((state) => state.LoginReducer);
  const [modalAddShow, setModalAddShow] = useState(false);
  const [modalDetailShow, setModalDetailShow] = useState(false);
  const [mount, setMount] = useState(false);

  useEffect(() => {
    getData();
    getPeriod();
    getMarketplace();
  }, []);

  const getData = () => {
    const header = {
      headers : {
        authorization: token
      }
    }
    axios.get(API_URL + 'api/v1/package', header)
    .then((response) => {      
      setPackageData( response.data.data);   
      setMount(true);  
    })
    .catch(function (error) {
      Swal.fire({
        icon: 'error',
        title: 'Something Went Wrong',
        text: error,
      });
    })
  }

  const getPeriod = () => {
    const header = {
      headers : {
        authorization: token
      }
    }
    axios.get(API_URL + 'api/v1/periode', header)
    .then((response) => {   
      setPeriodData(response.data.data);     
    })
  }

  const getMarketplace = () => {
    const header = {
      headers : {
        authorization: token
      }
    }
    axios.get(API_URL + 'api/v1/marketplace', header)
    .then((response) => {   
      setMarketplaceData(response.data.data);     
    })
  }

  const handlerDetail = (dataDetail) => {
    setModalDetailShow(true);
    setPackageDetail(dataDetail);
  }

  const handlerEdit = (dataEdit) => {
    setModalAddShow(true);
    setPackageEdit(dataEdit);
    setIsUpdate(true);
  }

  return (
    <Page title="Daftar Paket" breadcrumbs={[{ name: 'Paket', active: true }]}>

      <Card className="p-2 border-0" style={{ borderRadius : '1rem', boxShadow: '0 0.5rem 1rem rgb(0 0 0 / 5%)' }}>
        <Card.Body>
          <Row>
            <Col className="text-right">
                <Button variant="outline-primary" onClick={() => setModalAddShow(true)}>
                  <MdAddCircleOutline /> Tambah
                </Button>
            </Col>
          </Row>
          
          <Row>
            <Col>
              <Table hover className="text-center" style={{ fontSize: 12 }}>
                <thead>
                  <tr>
                    <th>No</th>
                    <th>Kode Paket</th>
                    <th>Nama Paket</th>
                    <th>Marketplace</th>
                    <th>Periode Paket</th>
                    <th>Berlaku Dari</th>
                    <th>Berlaku Sampai</th>
                    <th>Harga (Rp)</th>
                    <th>Status</th>
                    <th>Created Date</th>
                    <th>Action</th>
                  </tr>
                </thead>
                <tbody>
                  {
                    mount ? (
                      packageData.map((val, key) => {
                        return(<tr key={"package_id" + key}>
                          <td className="align-middle">{key += 1}</td>
                          <td className="align-middle">{val.package_code}</td>
                          <td className="align-middle">{val.package_name}</td>
                          <td className="align-middle">{val.marketplace_name}</td>
                          <td className="align-middle">{val.periode_name}</td>
                          <td className="align-middle">{val.start_date}</td>
                          <td className="align-middle">{val.end_date}</td>
                          <td className="align-middle">{nf.format(val.price)}</td>
                          <td className="align-middle">{val.status === 1 ? "Aktif" : "Nonaktif" }</td>
                          <td className="align-middle">{val.created_date}</td>
                          <td className='d-flex justify-content-between border-0'>
                            <Button size="sm" variant="warning" className="mr-1" onClick={() => handlerEdit(val)}>
                              <MdEdit />
                            </Button>
                            <Button size="sm" variant="success" onClick={() => handlerDetail(val)}>
                              <MdList />
                            </Button>
                          </td>
                        </tr>)
                      })
                    ) : (
                      <tr>
                        <td className='align-middle' colSpan={11}>Loading..</td>
                      </tr>
                    )
                  }
                              
                </tbody>
              </Table>  

              <ModalAddPackage
                isupdate = {isUpdate.toString()}
                show ={ modalAddShow}
                onHide={() => {
                  setModalAddShow(false);
                  setIsUpdate(false);
                  setPackageEdit('');
                  getData();
                }}
                token = {token}
                perioddata = {periodData}
                marketplacedata = {marketplaceData}
                data = {packageEdit}
              />

              <ModalDetailPackage
                show={modalDetailShow}
                onHide = {() => {
                  setModalDetailShow(false);
                  getData();
                }}
                token={token}
                data={packageDetail}
              />

            </Col>
          </Row>  

        </Card.Body>
      </Card>   

    </Page>
  );
};

export default AdminPackage;
