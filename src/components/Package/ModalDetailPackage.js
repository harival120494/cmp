import React from "react";
import Swal from "sweetalert2";
import { API_URL, nf } from "../../utils/constants";
import axios from "axios";
import {
    Modal,
    Button
} from 'react-bootstrap';
import { 
    MdToggleOff,
    MdToggleOn
  } from 'react-icons/md';

function ModalDetailPackage(props) {

    const token = props.token;
    const data = props.data;
    const id = props.data.package_id;
    const name = props.data.package_name;
    
    let status;
    if(data.status === 1) {
        status = "Aktif";
    } else {
        status = "Nonaktif";
    }

    let renderButton;
    if (data.status === 1) {
        renderButton = <Button variant="outline-dark" onClick={() => inActivateData()}>
                            <MdToggleOff />
                        </Button>;
      } else {
        renderButton = <Button variant="outline-dark" onClick={() => activateData()}>
                            <MdToggleOn />
                        </Button>;
      }

    const activateData = () => {
        Swal.fire({
            title: 'Konfirmasi',
            text: "Apakah Anda Yakin Akan Mengaktifkan " + name + "?",
            showCancelButton: true,
            confirmButtonText: 'Aktifkan',
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            cancelButtonText: 'Batal',
            reverseButtons: true
        }).then((result) => {
            if (result.value) {
            const header = {
                headers : {
                    authorization: token
                }
            }
            const data = {
                package_id : id,
                status: 1
            }
            axios.post(API_URL + 'api/v1/package/change-status', data, header )
            .then(() => {      
                Swal.fire({
                icon: 'success',
                title: name + ' Berhasil Diaktifkan',
                showConfirmButton: false,
                timer: 1000
                }) 
                .then( () => {
                    props.onHide();
                }); 
            })
            .catch(function (error) {
                Swal.fire({
                icon: 'error',
                title: 'Something Went Wrong',
                text: error,
                });
            })
            }
        })
    }
    const inActivateData = () => {
        Swal.fire({
            title: 'Konfirmasi',
            text: "Apakah Anda Yakin Akan Menonaktifkan " + name + "?",
            showCancelButton: true,
            confirmButtonText: 'Nonaktifkan',
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            cancelButtonText: 'Batal',
            reverseButtons: true
        }).then((result) => {
            if (result.value) {
            const header = {
                headers : {
                authorization: token
                }
            }
            const data = {
                package_id : id,
                status: 0
            }
            axios.post(API_URL + 'api/v1/package/change-status', data, header)
            .then(() => {      
                Swal.fire({
                icon: 'success',
                title: name + ' Berhasil Dinonaktifkan',
                showConfirmButton: false,
                timer: 1000
                }) 
                .then( () => {
                    props.onHide();
                }); 
            })
            .catch(function (error) {
                Swal.fire({
                icon: 'error',
                title: 'Something Went Wrong',
                text: error,
                });
            })
            }
        })
    }

    
    return (
        <Modal
            {...props}
            size="lg"
            aria-labelledby="contained-modal-title-vcenter"
            centered
        >           
            <Modal.Header closeButton>
                <Modal.Title id="contained-modal-title-vcenter">
                    Detail Paket
                </Modal.Title>
            </Modal.Header>
            <Modal.Body>

                <div className="px-4">

                    <div className="row py-3">
                        <div className="col-md-3 font-weight-bold">
                            Kode Paket
                        </div>
                        <div className="col-md-9">
                            {data.package_code}
                        </div>
                    </div>

                    <div className="row py-3">
                        <div className="col-md-3 font-weight-bold">
                            Periode Paket
                        </div>
                        <div className="col-md-9">
                            {data.periode_name}
                        </div>
                    </div>

                    <div className="row py-3">
                        <div className="col-md-3 font-weight-bold">
                            Berlaku
                        </div>
                        <div className="col-md-9 row">
                            <div className="col-md-6 row">
                                <div className="col-md-4 font-weight-bold">
                                    Dari
                                </div>
                                <div className="col-md-8">
                                    {data.start_date}
                                </div>                                
                            </div>
                            <div className="col-md-6 row">
                                <div className="col-md-4 font-weight-bold">
                                    Sampai
                                </div>
                                <div className="col-md-8">
                                    {data.end_date}
                                </div> 
                            </div>
                        </div>
                    </div>
                    
                    <div className="row py-3">
                        <div className="col-md-3 font-weight-bold">
                            Marketplace
                        </div>
                        <div className="col-md-9">
                            {data.marketplace_name}
                        </div>
                    </div>
                    
                    <div className="row py-3">
                        <div className="col-md-3 font-weight-bold">
                            Harga
                        </div>
                        <div className="col-md-9">
                            Rp {nf.format(data.price)}
                        </div>
                    </div>
                    
                    <div className="row py-3">
                        <div className="col-md-3 font-weight-bold">
                            Deskripsi
                        </div>
                        <div className="col-md-9">
                            {data.paket_desc}
                        </div>
                    </div>
                    
                    <div className="row py-3">
                        <div className="col-md-3 font-weight-bold">
                            Status
                        </div>
                        <div className="col-md-9">
                            { renderButton }  {status}
                        </div>
                    </div>

                </div>
                           
            </Modal.Body>
            <Modal.Footer>
                <Button variant="outline-danger" onClick={props.onHide}>Kembali</Button>          
            </Modal.Footer>
        </Modal>
    );
}

export default ModalDetailPackage;