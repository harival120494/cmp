import React, {useState} from "react";
import Swal from "sweetalert2";
import { API_URL } from "../../utils/constants";
import axios from "axios";
import {
    Modal,
    Button
} from 'react-bootstrap';
import { CKEditor } from '@ckeditor/ckeditor5-react';
import ClassicEditor from '@ckeditor/ckeditor5-build-classic';

function ModalAddPackage(props) {

    const data = props.data;
    const id = props.data.package_id;
    const token = props.token;
    const periodData = props.perioddata;
    const marketplaceData = props.marketplacedata;

    const [periode, setPeriode] = useState('');
    const [dari, setDari] = useState('');
    const [sampai, setSampai] = useState('');
    const [marketplace, setMarketplace] = useState('');
    const [nama, setNama] = useState('');
    const [harga, setHarga] = useState('');
    const [deskripsi, setDeskripsi] = useState('');

    let modalTitle;
    if(props.isupdate === "true") {
        modalTitle = "Ubah Paket";
    } else {
        modalTitle = "Tambah Paket";
    }

    const handleSubmit = async (e) => {
        e.preventDefault();

        if (periode === '') {
            ErrorAlert("Periode Paket Harus Diisi");
            return false;
        }
        if (dari === '') {
            ErrorAlert("Berlaku Dari Harus Diisi");
            return false;
        }
        if (sampai === '') {
            ErrorAlert("Berlaku Sampai Harus Diisi");
            return false;
        }
        if (marketplace === '') {
            ErrorAlert("Marketplace Harus Diisi");
            return false;
        }
        if (nama === '') {
            ErrorAlert("Nama Paket Harus Diisi");
            return false;
        }
        if (harga === '') {
            ErrorAlert("Harga Harus Diisi");
            return false;
        }
        if (deskripsi === '') {
            ErrorAlert("Deskripsi Harus Diisi");
            return false;
        }

        const data = {
            periode_id: periode,
            start_date: dari,
            end_date: sampai,
            marketplace_id: marketplace,
            package_name: nama,
            price: harga,
            desc: deskripsi,
            status: 1
        }
        
        const header = {
            headers : {
                authorization: token
            }
        }
        await axios.post(API_URL + 'api/v1/package', data, header)
        .then((response) => {
            if(response.data.status === 201) {
                Swal.fire({
                    icon: 'success',
                    title: 'Data Berhasil Disimpan',
                    showConfirmButton: false,
                    timer: 1000
                })
                .then( () => {
                    props.onHide();
                    setEmptyForm();
                });          
            } else if (response.data.status === 204) {
                Swal.fire({
                    icon: 'error',
                    title: 'Error',
                    text: response.data.message
                })
                .then( () => {
                    setEmptyForm();
                }); 
            }
            
        })
        .catch(function (error) {
            Swal.fire({
                icon: 'error',
                title: 'Something Went Wrong',
                text: error,
              });
        });
    };

    const handleUpdate = async (e) => {
        e.preventDefault();

        let dataPeriode;
        let dataDari;
        let dataSampai;
        let dataMarketplace;
        let dataNama;
        let dataHarga;
        let dataDeskripsi;

        if (periode) {
            dataPeriode = periode;
        } else {
            dataPeriode = props.data.periode_id;
        }
        if (dari) {
            dataDari = dari;
        } else {
            dataDari = props.data.start_date;
        }
        if (sampai) {
            dataSampai = sampai;
        } else {
            dataSampai = props.data.end_date;
        }
        if (marketplace) {
            dataMarketplace = marketplace;
        } else {
            dataMarketplace = props.data.marketplace_id;
        }
        if (nama) {
            dataNama = nama;
        } else {
            dataNama = props.data.package_name;
        }
        if (harga) {
            dataHarga = harga;
        } else {
            dataHarga = props.data.price;
        }
        if (deskripsi) {
            dataDeskripsi = deskripsi;
        } else {
            dataDeskripsi = props.data.paket_desc;
        }


        if (dataPeriode === '') {
            ErrorAlert("Periode Paket Harus Diisi");
            return false;
        }
        if (dataDari === '') {
            ErrorAlert("Berlaku Dari Harus Diisi");
            return false;
        }
        if (dataSampai === '') {
            ErrorAlert("Berlaku Sampai Harus Diisi");
            return false;
        }
        if (dataMarketplace === '') {
            ErrorAlert("Marketplace Harus Diisi");
            return false;
        }
        if (dataNama === '') {
            ErrorAlert("Nama Paket Harus Diisi");
            return false;
        }
        if (dataHarga === '') {
            ErrorAlert("Harga Harus Diisi");
            return false;
        }
        if (dataDeskripsi === '') {
            ErrorAlert("Deskripsi Harus Diisi");
            return false;
        }

        const data = {
            periode_id: dataPeriode,
            start_date: dataDari,
            end_date: dataSampai,
            marketplace_id: dataMarketplace,
            package_name: dataNama,
            price: dataHarga,
            desc: dataDeskripsi,
            status: 1
        }
        
        const header = {
            headers : {
                authorization: token
            }
        }
        await axios.put(API_URL + 'api/v1/package/' + id, data, header)
        .then((response) => {
            if(response.data.status === 200) {
                Swal.fire({
                    icon: 'success',
                    title: 'Data Berhasil Disimpan',
                    showConfirmButton: false,
                    timer: 1000
                })
                .then( () => {
                    props.onHide();
                    setEmptyForm();
                });          
            } else if (response.data.status === 204) {
                Swal.fire({
                    icon: 'error',
                    title: 'Error',
                    text: response.data.message
                })
                .then( () => {
                    setEmptyForm();
                }); 
            }
            
        })
        .catch(function (error) {
            Swal.fire({
                icon: 'error',
                title: 'Something Went Wrong',
                text: error,
            });
        });
    };

    const setEmptyForm = () => {
        setPeriode('');
        setDari('');
        setSampai('');
        setMarketplace('');
        setNama('');
        setHarga('');
        setDeskripsi('');
    }

    const ErrorAlert = (string) => {
        Swal.fire({
            icon: 'error',
            title: 'Oops',
            text: string,
        });
    };
    
    return (
        <Modal
            {...props}
            size="lg"
            aria-labelledby="contained-modal-title-vcenter"
            centered
        >           
            <Modal.Header closeButton>
                <Modal.Title id="contained-modal-title-vcenter">
                    { modalTitle }
                </Modal.Title>
            </Modal.Header>
            <Modal.Body>
                <form  onSubmit={handleSubmit} className="px-4">                    
                    <div className="form-group row">
                        <label className="col-md-3 col-form-label">Periode Paket</label>
                        <div className="col-md-9">
                            <select className="form-control" onChange={(e) => setPeriode(e.target.value)} defaultValue={ props.isupdate === "true" ? data.periode_id : '' }>
                                <option>- Pilih Periode Paket -</option>
                                {periodData.map((v, k) => {
                                    return(
                                        <option key={"periode_id" + k} value={v.periode_id}>{v.periode_name}</option>
                                    )
                                })}                                
                            </select>
                        </div>
                    </div>

                    <div className="row">
                        <label className="col-md-3 col-form-label">Masa Berlaku</label>
                        <div className="col-md-9">
                            <div className="row">
                                <div className="col-md-6">
                                    <div className="form-group row">
                                        <label className="col-4 col-form-label">Dari</label>
                                        <div className="col-8">
                                            <input type="date" className="form-control"
                                            onChange={(e) => setDari(e.target.value)}
                                            defaultValue={ props.isupdate === "true" ? data.start_date : '' } />
                                        </div>
                                    </div>
                                </div>
                                <div className="col-md-6">
                                        <div className="form-group row">
                                            <label className="col-4 col-form-label">Sampai</label>
                                            <div className="col-8">
                                                <input type="date" className="form-control" onChange={(e) => setSampai(e.target.value)}
                                                defaultValue={ props.isupdate === "true" ? data.end_date : '' } />
                                            </div>
                                        </div>
                                </div>
                            </div>
                        </div>                        
                    </div>
                    
                    <div className="form-group row">
                        <label className="col-md-3 col-form-label">Marketplace</label>
                        <div className="col-md-9">
                            <select className="form-control" onChange={(e) => setMarketplace(e.target.value)} defaultValue={ props.isupdate === "true" ? data.marketplace_id : '' }>
                                <option>- Pilih Marketplace -</option>
                                {marketplaceData.map((v, k) => {
                                    return(
                                        <option key={"marketplace_id" + k} value={v.marketplace_id}>{v.marketplace_name}</option>
                                    )
                                })} 
                            </select>
                        </div>
                    </div>

                    <div className="form-group row">
                        <label className="col-md-3 col-form-label">Nama Paket</label>
                        <div className="col-md-9">
                            <input type="text" className="form-control" placeholder="Nama Paket"
                            onChange={(e) => setNama(e.target.value)}
                            defaultValue={ props.isupdate === "true" ? data.package_name : '' } />
                        </div>
                    </div>  

                    <div className="form-group row">
                        <label className="col-md-3 col-form-label">Harga (Rp)</label>
                        <div className="col-md-9">
                            <input type="number" className="form-control" placeholder="Harga Paket"
                            onChange={(e) => setHarga(e.target.value)}
                            defaultValue={ props.isupdate === "true" ? data.price : '' } />
                        </div>
                    </div>   

                    <div className="form-group row">
                        <label className="col-md-3 col-form-label">Deskripsi</label>
                        <div className="col-md-9">
                            <CKEditor
                                editor={ ClassicEditor }
                                onChange={
                                    (e, data) => setDeskripsi(data.getData())
                                }
                                data={props.isupdate === "true" ? data.paket_desc : '' }
                            />
                        </div>
                    </div>    
                </form>
               
            </Modal.Body>
            <Modal.Footer>
                <Button variant="outline-danger" onClick={props.onHide}>Batal</Button>
                <Button variant="primary mr-4" onClick={ props.isupdate === "true" ? handleUpdate : handleSubmit }>Simpan</Button>            
            </Modal.Footer>
        </Modal>
    );
}

export default ModalAddPackage;