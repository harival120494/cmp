import React, {useState, useEffect} from "react";
import Swal from "sweetalert2";
import { API_URL } from "../../utils/constants";
import axios from "axios";
import {
    Modal,
    Button
} from 'react-bootstrap';
import { CKEditor } from '@ckeditor/ckeditor5-react';
import ClassicEditor from '@ckeditor/ckeditor5-build-classic';

function ModalAddRole(props) {
    console.log(props);

    // if(props.onHide) {
    //     setEmptyForm();
    // }

    useEffect(() => {
        // setEmptyForm();
        if(props.isupdate === "true") {
            // setEmptyForm();
            setNama('');
            setDeskripsi('');
        }
    }, [])

    const data = props.data;
    const id = data.id_role;

    const [nama, setNama] = useState('');
    const [deskripsi, setDeskripsi] = useState('');

    // if(props.isupdate === "true") {
    //     // setEmptyForm();
    //     setNama('');
    //     setDeskripsi('');
    // }

    console.log(nama)

    let modalTitle;
    if(props.isupdate === "true") {
        modalTitle = "Ubah Role";
    } else {
        modalTitle = "Tambah Role";
    }

    const handleSubmit = async (e) => {
        
        if (nama === '') {
            ErrorAlert("Nama Role Harus Diisi");
            return false;
        }
        if (deskripsi === '') {
            ErrorAlert("Deskripsi Harus Diisi");
            return false;
        }

        const data = {
            role_name: nama,
            desc: deskripsi,
            status: 1
        }
        
        const header = {
            headers : {
                authorization: props.token
            }
        }
        await axios.post(API_URL + 'api/v1/role', data, header)
        .then((response) => {
            if(response.data.status === 200) {
            Swal.fire({
                icon: 'success',
                title: 'Data Berhasil Disimpan',
                showConfirmButton: false,
                timer: 1000
            })
            .then( () => {
                props.onHide();
                setEmptyForm();
            });          
            } else if (response.data.status === 204) {
                Swal.fire({
                    icon: 'error',
                    title: 'Error',
                    text: response.data.message
                })
                .then( () => {
                    setEmptyForm();
                }); 
            }
            
        })
        .catch(function (error) {
            Swal.fire({
                icon: 'error',
                title: 'Something Went Wrong',
                text: error
            })
            .then( () => {
                setEmptyForm();
            }); 
        });
    };

    const handleUpdate = async (e) => {
        let dataNama;
        let dataDesc;

        if (nama) {
            dataNama = nama;
        } else {
            dataNama = props.data.role_name;
        }

        if (deskripsi) {
            dataDesc = deskripsi
        } else {
            dataDesc = props.data.desc
        }
        
        if (dataNama === '') {
            ErrorAlert("Nama Role Harus Diisi");
            return false;
        }
        if (dataDesc === '') {
            ErrorAlert("Deskripsi Harus Diisi");
            return false;
        }

        const data = {
            role_name: dataNama,
            desc: dataDesc,
            status: 1
        }
        
        const header = {
            headers : {
                authorization: props.token
            }
        }
        await axios.put(API_URL + 'api/v1/role/' + id, data, header)
        .then((response) => {
            if(response.data.status === 200) {
            Swal.fire({
                icon: 'success',
                title: 'Data Berhasil Diperbarui',
                showConfirmButton: false,
                timer: 1000
            })
            .then( () => {
                props.onHide();
                setEmptyForm();
            });          
            } else if (response.data.status === 204) {
                Swal.fire({
                    icon: 'error',
                    title: 'Error',
                    text: response.data.message
                })
                .then( () => {
                    setEmptyForm();
                }); 
            }
            
        })
        .catch(function (error) {
            Swal.fire({
                icon: 'error',
                title: 'Something Went Wrong',
                text: error
            })
            .then( () => {
                setEmptyForm();
            });
        });
    };

    const setEmptyForm = () => {
        setNama('');
        setDeskripsi('');
    }

    const ErrorAlert = (string) => {
        Swal.fire({
            icon: 'error',
            title: 'Oops',
            text: string,
        });
    };
    
   
    return (
        <Modal
            {...props}
            size="lg"
            aria-labelledby="contained-modal-title-vcenter"
            centered
        >           
            <Modal.Header closeButton>
                <Modal.Title id="contained-modal-title-vcenter">
                    { modalTitle }
                </Modal.Title>
            </Modal.Header>
            <Modal.Body>
                <form className="px-4">                   

                    <div className="form-group row">
                        <label className="col-md-3 col-form-label">Nama Role</label>
                        <div className="col-md-9">
                            <input type="text" className="form-control" placeholder="Nama Role" onChange={(e) => setNama(e.target.value)} defaultValue={ props.isupdate === "true" ? data.role_name : '' } />
                        </div>
                    </div>    

                    <div className="form-group row">
                        <label className="col-md-3 col-form-label">Deskripsi</label>
                        <div className="col-md-9">
                            <CKEditor
                                editor={ ClassicEditor }
                                onChange={
                                    (e, data) => setDeskripsi(data.getData())
                                }
                                data={ props.isupdate === "true" ? data.desc : '' } 
                            />
                        </div>
                    </div>    
                </form>
               
            </Modal.Body>
            <Modal.Footer>
                <Button variant="outline-danger" onClick={props.onHide}>Batal</Button>
                <Button variant="primary mr-4" onClick={ props.isupdate === "true" ? handleUpdate : handleSubmit }>Simpan</Button>            
            </Modal.Footer>
        </Modal>
    );
}

export default ModalAddRole;