import logo200Image from 'assets/img/logo/logo_200.png';
// import sidebarBgImage from 'assets/img/sidebar/sidebar-4.jpg';
import SourceLink from 'components/SourceLink';
import React, {useState} from 'react';
import { FaUser } from 'react-icons/fa';
import {
  MdBorderAll,
  MdDashboard,
  MdExtension,
  MdInsertChart,
  MdKeyboardArrowDown,
  MdTextFields,
  MdWeb,
  MdWidgets,
  MdStore,
  MdViewList,
  MdOutlinePeopleOutline,
  MdOutlinePersonOutline,
} from 'react-icons/md';
import { NavLink } from 'react-router-dom';
import {
  Collapse,
  Nav,
  Navbar,
  NavItem,
  NavLink as BSNavLink,
} from 'reactstrap';
import bn from 'utils/bemnames';

const sidebarBackground = {
  // backgroundImage: `url("${sidebarBgImage}")`,
  backgroundSize: 'cover',
  backgroundRepeat: 'no-repeat',
};

const navItems = [
  { to: '/', name: 'Dashboard', exact: true, Icon: MdDashboard },
  { to: '/marketplace', name: 'Marketplace', exact: true, Icon: MdStore },
  { to: '/package', name: 'Package', exact: true, Icon: MdStore },
  { to: '/periode', name: 'Periode', exact: true, Icon: MdStore },
  { to: '/role', name: 'Role', exact: true, Icon: MdStore },
  { to: '/cards', name: 'Cards', exact: false, Icon: MdWeb },
  { to: '/charts', name: 'Charts', exact: false, Icon: MdInsertChart },
  { to: '/widgets', name: 'Widgets', exact: false, Icon: MdWidgets },
];


const navMasterDataAdmin = [
  { to: '/store-profile', name: 'Profil Toko', exact: true, Icon: MdViewList },
  { to: '/product', name: 'Produk', exact: true, Icon: MdBorderAll },
  { to: '/promotion', name: 'Promosi', exact: true, Icon: MdBorderAll },
  { to: '/customer', name: 'Pelanggan', exact: true, Icon: MdBorderAll },
  { to: '/package', name: 'Paket', exact: true, Icon: MdBorderAll },
];

const navConfigAdmin = [
  { to: '/marketplace', name: 'Marketplace', exact: true, Icon: MdTextFields },
  { to: '/periode', name: 'Periode', exact: true, Icon: MdTextFields },
  { to: '/bank', name: 'Bank', exact: true, Icon: MdTextFields },
  { to: '/role', name: 'Role', exact: true, Icon: MdTextFields }
];

const navManajemenProdukAdmin = [
  { to: '/stock-management', name: 'Pengaturan Stok', exact: true, Icon: MdTextFields },
  { to: '/stock-history', name: 'Riwayat Stok', exact: true, Icon: MdTextFields }
];

const navManajemenTokoAdmin = [
  { to: '/store', name: 'Daftar Toko', exact: true, Icon: MdTextFields }
];

const navManajemenUserAdmin = [
  { to: '/user', name: 'User', exact: true, Icon: MdOutlinePersonOutline }
];

const bem = bn.create('sidebar');

function SidebarSuperAdmin(){

  const [isOpenComponents, setOpenComponents] = useState(true);
  const [isOpenContents, setOpenContents] = useState(true);
  const [isOpenPages, setOpenPages] = useState(true);

  // super-admin
  const [isOpenMasterDataAdmin, setOpenMasterDataAdmin] = useState(false);
  const [isOpenConfigAdmin, setOpenConfigAdmin] = useState(false);
  const [isOpenManajemenProdukAdmin, setOpenManajemenProdukAdmin] = useState(false);
  const [isOpenManajemenTokoAdmin, setOpenManajemenTokoAdmin] = useState(false);
  const [isOpenManajemenUserAdmin, setOpenManajemenUserAdmin] = useState(false);


  const handleClick = (name) => {
    switch (name) {
      case 'Components':
        setOpenComponents(!isOpenComponents)
        break;
      case 'Contents':
          setOpenContents(!isOpenContents)
          break;
      case 'Pages':
          setOpenPages(!isOpenPages)
          break;
      case 'MasterDataAdmin':
          setOpenMasterDataAdmin(!isOpenMasterDataAdmin)
          break;
      case 'ConfigAdmin':
          setOpenConfigAdmin(!isOpenConfigAdmin)
          break;
      case 'ManajemenProdukAdmin':
          setOpenManajemenProdukAdmin(!isOpenManajemenProdukAdmin)
          break;
      case 'ManajemenTokoAdmin':
          setOpenManajemenTokoAdmin(!isOpenManajemenTokoAdmin)
          break;
      case 'ManajemenUserAdmin':
          setOpenManajemenUserAdmin(!isOpenManajemenUserAdmin)
          break;
      default:
        break;
    }
  };


  return (
    <aside className={bem.b()}>
      <div className={bem.e('background')} style={sidebarBackground} />
      <div className={bem.e('content')}>
        <Navbar>
          <SourceLink className="navbar-brand d-flex">
            <img
              src={logo200Image}
              width="40"
              height="30"
              className="pr-2"
              alt=""
            />
            <span className="text-white">
              CMP <FaUser />
            </span>
          </SourceLink>
        </Navbar>


        <Nav vertical>

          <NavItem className={bem.e('nav-item')}>
            <BSNavLink
              id={`navItem-Dashboard`}
              tag={NavLink}
              to="/"
              activeClassName="active"
              exact={true}
            >
              <MdDashboard className={bem.e('nav-item-icon')} />
              <span className="">DASHBOARD</span>
            </BSNavLink>
          </NavItem>


          <NavItem
            className={bem.e('nav-item')}
            onClick={() => handleClick('MasterDataAdmin')}
          >
            <BSNavLink className={bem.e('nav-item-collapse')}>
              <div className="d-flex">
                <MdViewList className={bem.e('nav-item-icon')} />
                <span className=" align-self-start">MASTER DATA</span>
              </div>
              <MdKeyboardArrowDown
                className={bem.e('nav-item-icon')}
                style={{
                  padding: 0,
                  transform: isOpenMasterDataAdmin
                    ? 'rotate(0deg)'
                    : 'rotate(-90deg)',
                  transitionDuration: '0.3s',
                  transitionProperty: 'transform',
                }}
              />
            </BSNavLink>
          </NavItem>
          <Collapse isOpen={isOpenMasterDataAdmin}>
            {navMasterDataAdmin.map(({ to, name, exact, Icon }, index) => (
              <NavItem key={index} className={bem.e('nav-item')} style={{ marginLeft: '30px' }}>
                <BSNavLink
                  id={`navItem-${name}-${index}`}
                  tag={NavLink}
                  to={to}
                  activeClassName="active"
                  exact={exact}
                >
                  <Icon className={bem.e('nav-item-icon')} />
                  <span className="">{name}</span>
                </BSNavLink>
              </NavItem>
            ))}
          </Collapse> 


          <NavItem
            className={bem.e('nav-item')}
            onClick={() => handleClick('ConfigAdmin')}
          >
            <BSNavLink className={bem.e('nav-item-collapse')}>
              <div className="d-flex">
                <MdExtension className={bem.e('nav-item-icon')} />
                <span className=" align-self-start">CONFIG</span>
              </div>
              <MdKeyboardArrowDown
                className={bem.e('nav-item-icon')}
                style={{
                  padding: 0,
                  transform: isOpenConfigAdmin
                    ? 'rotate(0deg)'
                    : 'rotate(-90deg)',
                  transitionDuration: '0.3s',
                  transitionProperty: 'transform',
                }}
              />
            </BSNavLink>
          </NavItem>
          <Collapse isOpen={isOpenConfigAdmin}>
            {navConfigAdmin.map(({ to, name, exact, Icon }, index) => (
              <NavItem key={index} className={bem.e('nav-item')} style={{ marginLeft: '30px' }}>
                <BSNavLink
                  id={`navItem-${name}-${index}`}
                  tag={NavLink}
                  to={to}
                  activeClassName="active"
                  exact={exact}
                >
                  <Icon className={bem.e('nav-item-icon')} />
                  <span className="">{name}</span>
                </BSNavLink>
              </NavItem>
            ))}
          </Collapse> 
          

          <NavItem className={bem.e('nav-item')}>
            <BSNavLink
              id={`navItem-Dashboard`}
              tag={NavLink}
              to="/order"
              activeClassName="active"
              exact={true}
            >
              <MdDashboard className={bem.e('nav-item-icon')} />
              <span className="">PESANAN</span>
            </BSNavLink>
          </NavItem>
          

          <NavItem className={bem.e('nav-item')}>
            <BSNavLink
              id={`navItem-Dashboard`}
              tag={NavLink}
              to="/payment"
              activeClassName="active"
              exact={true}
            >
              <MdDashboard className={bem.e('nav-item-icon')} />
              <span className="">PEMBAYARAN</span>
            </BSNavLink>
          </NavItem>


          <NavItem
            className={bem.e('nav-item')}
            onClick={() => handleClick('ManajemenProdukAdmin')}
          >
            <BSNavLink className={bem.e('nav-item-collapse')}>
              <div className="d-flex">
                <MdExtension className={bem.e('nav-item-icon')} />
                <span className=" align-self-start">MANAJEMEN PRODUK</span>
              </div>
              <MdKeyboardArrowDown
                className={bem.e('nav-item-icon')}
                style={{
                  padding: 0,
                  transform: isOpenConfigAdmin
                    ? 'rotate(0deg)'
                    : 'rotate(-90deg)',
                  transitionDuration: '0.3s',
                  transitionProperty: 'transform',
                }}
              />
            </BSNavLink>
          </NavItem>
          <Collapse isOpen={isOpenManajemenProdukAdmin}>
            {navManajemenProdukAdmin.map(({ to, name, exact, Icon }, index) => (
              <NavItem key={index} className={bem.e('nav-item')} style={{ marginLeft: '30px' }}>
                <BSNavLink
                  id={`navItem-${name}-${index}`}
                  tag={NavLink}
                  to={to}
                  activeClassName="active"
                  exact={exact}
                >
                  <Icon className={bem.e('nav-item-icon')} />
                  <span className="">{name}</span>
                </BSNavLink>
              </NavItem>
            ))}
          </Collapse> 


          <NavItem
            className={bem.e('nav-item')}
            onClick={() => handleClick('ManajemenTokoAdmin')}
          >
            <BSNavLink className={bem.e('nav-item-collapse')}>
              <div className="d-flex">
                <MdExtension className={bem.e('nav-item-icon')} />
                <span className=" align-self-start">MANAJEMEN TOKO</span>
              </div>
              <MdKeyboardArrowDown
                className={bem.e('nav-item-icon')}
                style={{
                  padding: 0,
                  transform: isOpenConfigAdmin
                    ? 'rotate(0deg)'
                    : 'rotate(-90deg)',
                  transitionDuration: '0.3s',
                  transitionProperty: 'transform',
                }}
              />
            </BSNavLink>
          </NavItem>
          <Collapse isOpen={isOpenManajemenTokoAdmin}>
            {navManajemenTokoAdmin.map(({ to, name, exact, Icon }, index) => (
              <NavItem key={index} className={bem.e('nav-item')} style={{ marginLeft: '30px' }}>
                <BSNavLink
                  id={`navItem-${name}-${index}`}
                  tag={NavLink}
                  to={to}
                  activeClassName="active"
                  exact={exact}
                >
                  <Icon className={bem.e('nav-item-icon')} />
                  <span className="">{name}</span>
                </BSNavLink>
              </NavItem>
            ))}
          </Collapse> 


          <NavItem
            className={bem.e('nav-item')}
            onClick={() => handleClick('ManajemenUserAdmin')}
          >
            <BSNavLink className={bem.e('nav-item-collapse')}>
              <div className="d-flex">
                <MdOutlinePeopleOutline className={bem.e('nav-item-icon')} />
                <span className=" align-self-start">MANAJEMEN USER</span>
              </div>
              <MdKeyboardArrowDown
                className={bem.e('nav-item-icon')}
                style={{
                  padding: 0,
                  transform: isOpenConfigAdmin
                    ? 'rotate(0deg)'
                    : 'rotate(-90deg)',
                  transitionDuration: '0.3s',
                  transitionProperty: 'transform',
                }}
              />
            </BSNavLink>
          </NavItem>
          <Collapse isOpen={isOpenManajemenUserAdmin}>
            {navManajemenUserAdmin.map(({ to, name, exact, Icon }, index) => (
              <NavItem key={index} className={bem.e('nav-item')} style={{ marginLeft: '30px' }}>
                <BSNavLink
                  id={`navItem-${name}-${index}`}
                  tag={NavLink}
                  to={to}
                  activeClassName="active"
                  exact={exact}
                >
                  <Icon className={bem.e('nav-item-icon')} />
                  <span className="">{name}</span>
                </BSNavLink>
              </NavItem>
            ))}
          </Collapse> 
                              

          <NavItem className={bem.e('nav-item')}>
            <BSNavLink
              id={`navItem-Dashboard`}
              tag={NavLink}
              to="/report"
              activeClassName="active"
              exact={true}
            >
              <MdDashboard className={bem.e('nav-item-icon')} />
              <span className="">LAPORAN</span>
            </BSNavLink>
          </NavItem>

        
          {/* {navItems.map(({ to, name, exact, Icon }, index) => (
            <NavItem key={index} className={bem.e('nav-item')}>
              <BSNavLink
                id={`navItem-${name}-${index}`}
                tag={NavLink}
                to={to}
                activeClassName="active"
                exact={exact}
              >
                <Icon className={bem.e('nav-item-icon')} />
                <span className="">{name}</span>
              </BSNavLink>
            </NavItem>
          ))}       */}

        </Nav>

      </div>
    </aside>
  )

 
}

export default SidebarSuperAdmin;
