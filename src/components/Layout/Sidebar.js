import React from 'react';
import { useSelector } from 'react-redux';
import SidebarSuperAdmin from './SidebarSuperAdmin';
import SidebarUser from './SidebarUser';

function Sidebar(){
  
  const {role_id} = useSelector((state) => state.LoginReducer);
  console.log(role_id);


  let RenderComponent;

  if(role_id === 1) {
    RenderComponent = <SidebarSuperAdmin />
  } else {
    RenderComponent = <SidebarUser />
  }


  return (
    <>
      { RenderComponent }
    </>    
  )

 
}

export default Sidebar;
