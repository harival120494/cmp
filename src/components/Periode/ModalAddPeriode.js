import React, {useState} from "react";
import Swal from "sweetalert2";
import { API_URL } from "../../utils/constants";
import axios from "axios";
import {
    Modal,
    Button
} from 'react-bootstrap';

function ModalAddPeriode(props) {

    const data = props.data;
    const id = data.periode_id;

    const [nama, setNama] = useState('');
    const [jumlah_hari, setJumlahHari] = useState('');

    let modalTitle;
    if(props.isupdate === "true") {
        modalTitle = "Ubah Periode";
    } else {
        modalTitle = "Tambah Periode";
    }

    const handleSubmit = async (e) => {
        
        if (nama === '') {
            ErrorAlert("Nama Periode Harus Diisi");
            return false;
        }
        if (jumlah_hari === '') {
            ErrorAlert("Jumlah Hari Harus Diisi");
            return false;
        }

        const data = {
            periode_name: nama,
            jumlah_hari: jumlah_hari,
            status: 1
        }
        
        const header = {
            headers : {
                authorization: props.token
            }
        }
        await axios.post(API_URL + 'api/v1/periode', data, header)
        .then((response) => {
            if(response.data.status === 201) {
            Swal.fire({
                icon: 'success',
                title: 'Data Berhasil Disimpan',
                showConfirmButton: false,
                timer: 1000
            })
            .then( () => {
                props.onHide();
                setEmptyForm();
            });          
            } else if (response.data.status === 204) {
                Swal.fire({
                    icon: 'error',
                    title: 'Error',
                    text: response.data.message
                })
                .then( () => {
                    setEmptyForm();
                }); 
            }
            
        })
        .catch(function (error) {
            Swal.fire({
                icon: 'error',
                title: 'Something Went Wrong',
                text: error
            })
            .then( () => {
                setEmptyForm();
            }); 
        });
    };

    const handleUpdate = async (e) => {
        let dataNama;
        let dataJumlahHari;

        if (nama) {
            dataNama = nama;
        } else {
            dataNama = props.data.periode_name;
        }

        if (jumlah_hari) {
            dataJumlahHari = jumlah_hari
        } else {
            dataJumlahHari = props.data.jumlah_hari
        }
        
        if (dataNama === '') {
            ErrorAlert("Nama Periode Harus Diisi");
            return false;
        }
        if (dataJumlahHari === '') {
            ErrorAlert("Jumlah Hari Harus Diisi");
            return false;
        }

        const data = {
            periode_name: dataNama,
            jumlah_hari: dataJumlahHari,
            status: 1
        }
        
        const header = {
            headers : {
                authorization: props.token
            }
        }
        await axios.put(API_URL + 'api/v1/periode/' + id, data, header)
        .then((response) => {
            if(response.data.status === 200) {
            Swal.fire({
                icon: 'success',
                title: 'Data Berhasil Diperbarui',
                showConfirmButton: false,
                timer: 1000
            })
            .then( () => {
                props.onHide();
                setEmptyForm();
            });          
            } else if (response.data.status === 204) {
                Swal.fire({
                    icon: 'error',
                    title: 'Error',
                    text: response.data.message
                })
                .then( () => {
                    setEmptyForm();
                }); 
            }
            
        })
        .catch(function (error) {
            Swal.fire({
                icon: 'error',
                title: 'Something Went Wrong',
                text: error
            })
            .then( () => {
                setEmptyForm();
            });
        });
    };

    const setEmptyForm = () => {
        setNama('');
        setJumlahHari('');
    }

    const ErrorAlert = (string) => {
        Swal.fire({
            icon: 'error',
            title: 'Oops',
            text: string,
        });
    };
    
   
    return (
        <Modal
            {...props}
            size="lg"
            aria-labelledby="contained-modal-title-vcenter"
            centered
        >           
            <Modal.Header closeButton>
                <Modal.Title id="contained-modal-title-vcenter">
                    { modalTitle }
                </Modal.Title>
            </Modal.Header>
            <Modal.Body>
                <form className="px-4">                   

                    <div className="form-group row">
                        <label className="col-md-3 col-form-label">Nama Periode</label>
                        <div className="col-md-9">
                            <input type="text" className="form-control" placeholder="Nama Periode" onChange={(e) => setNama(e.target.value)} defaultValue={ props.isupdate === "true" ? data.periode_name : '' } />
                        </div>
                    </div>                   

                    <div className="form-group row">
                        <label className="col-md-3 col-form-label">Jumlah (Hari)</label>
                        <div className="col-md-9">
                            <input type="number" className="form-control" placeholder="Jumlah (Hari)" onChange={(e) => setJumlahHari(e.target.value)} defaultValue={ props.isupdate === "true" ? data.jumlah_hari : '' } />
                        </div>
                    </div>    
                      
                </form>
               
            </Modal.Body>
            <Modal.Footer>
                <Button variant="outline-danger" onClick={props.onHide}>Batal</Button>
                <Button variant="primary mr-4" onClick={ props.isupdate === "true" ? handleUpdate : handleSubmit }>Simpan</Button>            
            </Modal.Footer>
        </Modal>
    );
}

export default ModalAddPeriode;