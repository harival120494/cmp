import Page from 'components/Page';
import React, { useEffect, useState } from 'react';
import { API_URL } from '../../utils/constants';
import axios from 'axios';
import Swal from 'sweetalert2';
import { useSelector } from 'react-redux';
import ModalAddPeriode from './ModalAddPeriode';
import ModalDetailPeriode from './ModalDetailPeriode';
import {
  Col,
  Row,
  Table,
  Card,
  Button
} from 'react-bootstrap';
import { 
  MdList,
  MdEdit,
  MdAddCircleOutline
} from 'react-icons/md';


function AdminPeriode() {

  const [periodeData, setPeriodeData] = useState([]);
  const [periodeDetail, setRoleDetail] = useState({});
  const [periodeEdit, setPeriodeEdit] = useState({});

  const {token} = useSelector((state) => state.LoginReducer);
  const [modalAddShow, setModalAddShow] = useState(false);
  const [modalDetailShow, setModalDetailShow] = useState(false);
  const [isUpdate, setIsUpdate] = useState(false);
  const [mount, setMount] = useState(false);


  useEffect(() => {
    getData();  
  }, []);

  const getData = () => {
    const header = {
      headers : {
        authorization: token
      }
    }
    axios.get(API_URL + 'api/v1/periode', header)
    .then((response) => {   
      setPeriodeData(response.data.data);    
      setMount(true);    
    })
    .catch(function (error) {
      Swal.fire({
        icon: 'error',
        title: 'Something Went Wrong',
        text: error,
      });
    })
  }

  const handlerAdd = () => {
    setModalAddShow(true);
  }

  const handlerDetail = (dataDetail) => {
    setModalDetailShow(true);
    setRoleDetail(dataDetail);
  }

  const handlerEdit = (dataEdit) => {
    setModalAddShow(true);
    setPeriodeEdit(dataEdit);
    setIsUpdate(true);
  }

  return (
    <Page title="Daftar Periode" breadcrumbs={[{ name: 'List Periode', active: true }]}>
      
    <Card className="p-2 border-0" style={{ borderRadius : '1rem', boxShadow: '0 0.5rem 1rem rgb(0 0 0 / 5%)' }}>
      <Card.Body>
        <Row>
          <Col className="text-right">
                <Button variant="outline-primary" onClick={() => handlerAdd()}>
                  <MdAddCircleOutline /> Tambah
                </Button>
          </Col>
        </Row>
        <Row>
        <Col>

        <Table className="text-center" style={{ fontSize: 12 }}>
          <thead>
            <tr>
              <th>No</th>
              <th>Nama</th>
              <th>Jumlah (Hari)</th>
              <th>Status</th>
              <th>Action</th>
            </tr>
          </thead>
          <tbody>
            {
              mount ? (
                periodeData.map((val, key) => {
                  return(<tr key={"id_role" + key}>
                    <td>{key += 1}</td>
                    <td>{val.periode_name}</td>
                    <td>{val.jumlah_hari}</td>
                    <td>{val.status === 1 ? "Aktif" : "Nonaktif" }</td>
                    <td>
                      <Button size="sm" variant="warning" className="mr-1" onClick={() => handlerEdit(val)}>
                        <MdEdit />
                      </Button>
                      <Button size="sm" variant="success" onClick={() => handlerDetail(val)}>
                        <MdList />
                      </Button>
                    </td>
                  </tr>)
                })
              ) : (
                <tr>
                   <td className='align-middle' colSpan={6}>Loading..</td>
                </tr>
              )              
            }
                         
          </tbody>
        </Table>


        <ModalAddPeriode
            isupdate = {isUpdate.toString()}
            show = {modalAddShow}
            onHide = {() => {
              setModalAddShow(false);
              setIsUpdate(false);
              getData();
              setPeriodeEdit('');
            }}
            token = {token}
            data = {periodeEdit}
          />

        <ModalDetailPeriode
          show={modalDetailShow}
          onHide = {() => {
            setModalDetailShow(false);
            getData();
          }}
          token={token}
          data={periodeDetail}
        />

        </Col>
        </Row>   
      </Card.Body>
    </Card>
   

    </Page>
  );
};

export default AdminPeriode;
