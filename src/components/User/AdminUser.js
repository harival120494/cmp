import Page from 'components/Page';
import React, { useEffect, useState } from 'react';
import { API_URL } from '../../utils/constants';
import axios from 'axios';
import Swal from 'sweetalert2';
import { useSelector } from 'react-redux';
import ModalAddUser from './ModalAddUser';
import ModalEditUser from './ModalEditUser';
import {
  Col,
  Row,
  Table,
  Card,
  Button
} from 'react-bootstrap';
import { 
  MdEdit,
  MdAddCircleOutline
} from 'react-icons/md';


function AdminUser() {

  const [userData, setUserData] = useState([]);
  const [roleData, setRoleData] = useState([]);
  const [userEdit, setUserEdit] = useState({});

  const {token} = useSelector((state) => state.LoginReducer);
  const [modalAddShow, setModalAddShow] = useState(false);
  const [modalEditShow, setModalEditShow] = useState(false);
  const [isUpdate, setIsUpdate] = useState(false);
  const [mount, setMount] = useState(false);


  useEffect(() => {
    getData();  
    getRoleData();
  }, []);

  const getData = () => {
    const header = {
      headers : {
        authorization: token
      }
    }
    axios.get(API_URL + 'api/v1/user/user-list', header)
    .then((response) => {   
      setUserData(response.data.data);    
      setMount(true);    
    })
    .catch(function (error) {
      Swal.fire({
        icon: 'error',
        title: 'Something Went Wrong',
        text: error,
      });
    })
  }

  const getRoleData = () => {
    const header = {
      headers : {
        authorization: token
      }
    }
    axios.get(API_URL + 'api/v1/role', header)
    .then((response) => {   
      setRoleData(response.data.data);
    })
  }

  const handlerAdd = () => {
    setModalAddShow(true);
  }

  const handlerEdit = (dataEdit) => {
    setModalEditShow(true);
    setUserEdit(dataEdit);
  }

  return (
    <Page title="Daftar User" breadcrumbs={[{ name: 'List User', active: true }]}>
      
    <Card className="p-2 border-0" style={{ borderRadius : '1rem', boxShadow: '0 0.5rem 1rem rgb(0 0 0 / 5%)' }}>
      <Card.Body>
        <Row>
          <Col className="text-right">
                <Button variant="outline-primary" onClick={() => handlerAdd()}>
                  <MdAddCircleOutline /> Tambah
                </Button>
          </Col>
        </Row>
        <Row>
        <Col>

        <Table className="text-center" style={{ fontSize: 12 }}>
          <thead>
            <tr>
              <th>No</th>
              <th>Nama PIC</th>
              <th>Email</th>
              <th>No. Handphone</th>
              <th>Role</th>
              <th>Created Date</th>
              <th>Status</th>
              <th>Action</th>
            </tr>
          </thead>
          <tbody>
            {
              mount ? (
                userData.map((val, key) => {
                  return(<tr key={"user_id" + key}>
                    <td>{key += 1}</td>
                    <td>{val.pic_name}</td>
                    <td>{val.email}</td>
                    <td>{val.nohp}</td>
                    <td>{val.role_name}</td>
                    <td>{val.created_at}</td>
                    <td>{val.status === 1 ? "Aktif" : "Nonaktif" }</td>
                    <td>
                      <Button size="sm" variant="warning" className="mr-1" onClick={() => handlerEdit(val)}>
                        <MdEdit />
                      </Button>
                    </td>
                  </tr>)
                })
              ) : (
                <tr>
                   <td className='align-middle' colSpan={8}>Loading..</td>
                </tr>
              )              
            }
                         
          </tbody>
        </Table>


          <ModalAddUser
            isupdate = {isUpdate.toString()}
            show = {modalAddShow}
            onHide = {() => {
              setModalAddShow(false);
              setIsUpdate(false);
              getData();
              setUserEdit('');
            }}
            token = {token}
            roledata = {roleData}
          />
          
          <ModalEditUser
            isupdate = {isUpdate.toString()}
            show = {modalEditShow}
            onHide = {() => {
              setModalEditShow(false);
              setIsUpdate(false);
              getData();
              setUserEdit('');
            }}
            token = {token}
            data = {userEdit}
            roledata = {roleData}
          />

        </Col>
        </Row>   
      </Card.Body>
    </Card>
   

    </Page>
  );
};

export default AdminUser;
