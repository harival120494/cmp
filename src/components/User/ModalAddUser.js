import React, {useState} from "react";
import Swal from "sweetalert2";
import { API_URL } from "../../utils/constants";
import axios from "axios";
import {
    Modal,
    Button
} from 'react-bootstrap';
import {
    MdToggleOff,
    MdToggleOn
} from 'react-icons/md';

function ModalAddUser(props) {

    const roleData = props.roledata;

    const [nama, setNama] = useState('');
    const [email, setEmail] = useState('');
    const [nohp, setNohp] = useState('');
    const [password, setPassword] = useState('');
    const [repassword, setRepassword] = useState('');
    const [role, setRole] = useState('');
    const [status, setStatus] = useState('1');

    let showStatus;
    if(status === '1') {
        showStatus = "Aktif";
    } else {
        showStatus = "Nonaktif";
    }

    let renderButton;
    if (status === '1') {
        renderButton = <Button size="sm" variant="outline-dark" onClick={() => setStatus('0')}>
                            <MdToggleOff />
                        </Button>;
      } else {
        renderButton = <Button size="sm" variant="outline-dark" onClick={() => setStatus('1')}>
                            <MdToggleOn />
                        </Button>;
      }

    const handleSubmit = async (e) => {
        
        if (nama === '') {
            ErrorAlert("Nama PIC Harus Diisi");
            return false;
        }
        if (email === '') {
            ErrorAlert("Email Harus Diisi");
            return false;
        }
        if (nohp === '') {
            ErrorAlert("No. Handphone Harus Diisi");
            return false;
        }
        if (role === '') {
            ErrorAlert("Role Harus Diisi");
            return false;
        }
        if (password === '') {
            ErrorAlert("Password Harus Diisi");
            return false;
        }
        if (password.length < 8) {
            ErrorAlert("Password Minimal 8 Karakter");
            return false;
        }
        if (repassword === '') {
            ErrorAlert("Konfirmasi Password");
            return false;
        }
        if (repassword !== password) {
            ErrorAlert("Konfirmasi Password Salah");
            return false;
        }

        const data = {
            pic_name: nama,
            email: email,
            no_hp: nohp,
            password: password,
            confirm_password: repassword,
            role_id: role,
            status: status
        }

        console.log(data);
        // return false;
        
        const header = {
            headers : {
                authorization: props.token
            }
        }
        await axios.post(API_URL + 'api/v1/user/register', data, header)
        .then((response) => {
            if(response.data.status === 201) {
            Swal.fire({
                icon: 'success',
                title: 'Data Berhasil Disimpan',
                showConfirmButton: false,
                timer: 1000
            })
            .then( () => {
                props.onHide();
                setEmptyForm();
            });          
            } else if (response.data.status === 204) {
                Swal.fire({
                    icon: 'error',
                    title: 'Error',
                    text: response.data.message
                })
                .then( () => {
                    setEmptyForm();
                }); 
            }
            
        })
        .catch(function (error) {
            Swal.fire({
                icon: 'error',
                title: 'Something Went Wrong',
                text: error
            })
            .then( () => {
                props.onHide();
                setEmptyForm();
            }); 
        });
    };


    const setEmptyForm = () => {
        setNama('');
        setEmail('');
        setNohp('');
        setPassword('');
        setRepassword('');
        setRole('');
        // setStatus('');
    }

    const ErrorAlert = (string) => {
        Swal.fire({
            icon: 'error',
            title: 'Oops',
            text: string,
        });
    };
    
   
    return (
        <Modal
            {...props}
            size="lg"
            aria-labelledby="contained-modal-title-vcenter"
            centered
        >           
            <Modal.Header closeButton>
                <Modal.Title id="contained-modal-title-vcenter">
                    Tambah User
                </Modal.Title>
            </Modal.Header>
            <Modal.Body>

                <form className="px-4">                   

                    <div className="form-group row">
                        <label className="col-md-3 col-form-label">Nama PIC</label>
                        <div className="col-md-9">
                            <input type="text" className="form-control" placeholder="Nama PIC" onChange={(e) => setNama(e.target.value)} />
                        </div>
                    </div>                   

                    <div className="form-group row">
                        <label className="col-md-3 col-form-label">Email</label>
                        <div className="col-md-9">
                            <input type="email" className="form-control" placeholder="Email" onChange={(e) => setEmail(e.target.value)} />
                        </div>
                    </div>                   

                    <div className="form-group row">
                        <label className="col-md-3 col-form-label">No. Handphone</label>
                        <div className="col-md-9">
                            <input type="number" className="form-control" placeholder="No. Handphone" onChange={(e) => setNohp(e.target.value)} />
                        </div>
                    </div>  
                    
                    <div className="form-group row">
                        <label className="col-md-3 col-form-label">Role</label>
                        <div className="col-md-9">
                            <select className="form-control" onChange={(e) => setRole(e.target.value)} >
                                <option>- Pilih Role -</option>
                                {roleData.map((v, k) => {
                                    return(
                                        <option key={"id_role" + k} value={v.id_role}>{v.role_name}</option>
                                    )
                                })} 
                            </select>
                        </div>
                    </div>                   

                    <div className="form-group row">
                        <label className="col-md-3 col-form-label">Password</label>
                        <div className="col-md-9">
                            <input type="password" className="form-control" placeholder="Password" onChange={(e) => setPassword(e.target.value)} />
                        </div>
                    </div>                   

                    <div className="form-group row">
                        <label className="col-md-3 col-form-label">Konfirmasi Password</label>
                        <div className="col-md-9">
                            <input type="password" className="form-control" placeholder="Konfirmasi Password" onChange={(e) => setRepassword(e.target.value)} />
                        </div>
                    </div>                   

                    <div className="form-group row">
                        <label className="col-md-3 col-form-label">Status</label>
                        <div className="col-md-9">
                            { renderButton }  {showStatus}
                        </div>
                    </div>   
                    
                </form>
               
            </Modal.Body>
            <Modal.Footer>
                <Button variant="outline-danger" onClick={props.onHide}>Batal</Button>
                <Button variant="primary mr-4" onClick={ handleSubmit }>Simpan</Button>            
            </Modal.Footer>
        </Modal>
    );
}

export default ModalAddUser;