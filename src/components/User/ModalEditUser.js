import React, {useState} from "react";
import Swal from "sweetalert2";
import { API_URL } from "../../utils/constants";
import axios from "axios";
import {
    Modal,
    Button
} from 'react-bootstrap';
import {
    MdToggleOff,
    MdToggleOn
} from 'react-icons/md';

function ModalEditUser(props) {

    const data = props.data;
    const id = data.user_id;
    const roleData = props.roledata;

    const [nama, setNama] = useState('');
    const [email, setEmail] = useState('');
    const [nohp, setNohp] = useState('');
    const [role, setRole] = useState('');
    const [status, setStatus] = useState('');

    let showStatus;
    let renderButton;
    if (status === 1) {
        showStatus = "Nonaktifkan";
        renderButton = <Button size="sm" variant="outline-dark" onClick={() => setStatus(0)}>
                        <MdToggleOff />
                    </Button>;
    } else {
        showStatus = "Aktifkan";
        renderButton = <Button size="sm" variant="outline-dark" onClick={() => setStatus(1)}>
                        <MdToggleOn />
                    </Button>;
    }

    console.log(status)

    const handleUpdate = async (e) => {
        let dataNama;
        let dataEmail;
        let dataNohp;
        let dataRole;
        let dataStatus;

        if (nama) {
            dataNama = nama;
        } else {
            dataNama = props.data.pic_name;
        }

        if (email) {
            dataEmail = email;
        } else {
            dataEmail = props.data.email;
        }

        if (nohp) {
            dataNohp = nohp;
        } else {
            dataNohp = props.data.no_hp;
        }

        if (role) {
            dataRole = role;
        } else {
            dataRole = props.data.role_id;
        }

        if (status) {
            dataStatus = status;
        } else {
            dataStatus = props.data.status;
        }

        
        if (dataNama === '') {
            ErrorAlert("Nama PIC Harus Diisi");
            return false;
        }

        const data = {
            pic_name: dataNama,
            email: dataEmail,
            no_hp: dataNohp,
            password: "1234567890",
            confirm_password: "1234567890",
            role_id: dataRole,
            status: dataStatus
        }

        // console.log(nohp);
        // return false;
        
        const header = {
            headers : {
                authorization: props.token
            }
        }
        await axios.put(API_URL + 'api/v1/user/update/' + id, data, header)
        .then((response) => {
            if(response.data.status === 200) {
            Swal.fire({
                icon: 'success',
                title: 'Data Berhasil Diperbarui',
                showConfirmButton: false,
                timer: 1000
            })
            .then( () => {
                props.onHide();
                setEmptyForm();
            });          
            } else if (response.data.status === 204) {
                Swal.fire({
                    icon: 'error',
                    title: 'Error',
                    text: response.data.message
                })
                .then( () => {
                    setEmptyForm();
                }); 
            }
            
        })
        .catch(function (error) {
            Swal.fire({
                icon: 'error',
                title: 'Something Went Wrong',
                text: error
            })
            .then( () => {
                setEmptyForm();
            });
        });
    };

    const setEmptyForm = () => {
        setNama('');
        setEmail('');
        setNohp('');
        setRole('');
        setStatus('');
    }

    const ErrorAlert = (string) => {
        Swal.fire({
            icon: 'error',
            title: 'Oops',
            text: string,
        });
    };
    
   
    return (
        <Modal
            {...props}
            size="lg"
            aria-labelledby="contained-modal-title-vcenter"
            centered
        >           
            <Modal.Header closeButton>
                <Modal.Title id="contained-modal-title-vcenter">
                    Ubah User
                </Modal.Title>
            </Modal.Header>
            <Modal.Body>
                <form className="px-4">                   

                    <div className="form-group row">
                        <label className="col-md-3 col-form-label">Nama PIC</label>
                        <div className="col-md-9">
                            <input type="text" className="form-control" placeholder="Nama PIC" onChange={(e) => setNama(e.target.value)} defaultValue={ data.pic_name } />
                        </div>
                    </div> 
                                      

                    <div className="form-group row">
                        <label className="col-md-3 col-form-label">Email</label>
                        <div className="col-md-9">
                            <input type="email" className="form-control" placeholder="Email" onChange={(e) => setEmail(e.target.value)} defaultValue={ data.email } />
                        </div>
                    </div>                   

                    <div className="form-group row">
                        <label className="col-md-3 col-form-label">No. Handphone</label>
                        <div className="col-md-9">
                            <input type="number" className="form-control" placeholder="No. Handphone" onChange={(e) => setNohp(e.target.value)} defaultValue={ data.nohp } />
                        </div>
                    </div>     
                    
                    <div className="form-group row">
                        <label className="col-md-3 col-form-label">Role</label>
                        <div className="col-md-9">
                            <select className="form-control" onChange={(e) => setRole(e.target.value)} defaultValue={ data.role_id }>
                                <option>- Pilih Role -</option>
                                {roleData.map((v, k) => {
                                    return(
                                        <option key={"id_role" + k} value={v.id_role}>{v.role_name}</option>
                                    )
                                })} 
                            </select>
                        </div>
                    </div>                             

                    <div className="form-group row">
                        <label className="col-md-3 col-form-label">Status</label>
                        <div className="col-md-9">
                            { renderButton }  {showStatus}
                        </div>
                    </div>     

                </form>
               
            </Modal.Body>
            <Modal.Footer>
                <Button variant="outline-danger" onClick={props.onHide}>Batal</Button>
                <Button variant="primary mr-4" onClick={ handleUpdate }>Simpan</Button>            
            </Modal.Footer>
        </Modal>
    );
}

export default ModalEditUser;