import logo200Image from 'assets/img/logo/logo_200.png';
import PropTypes from 'prop-types';
import React, { useState, useEffect } from 'react';
import { Button, Form, FormGroup, Input, Label } from 'reactstrap';
import { API_URL } from '../utils/constants';
import axios from 'axios';
import { useDispatch } from "react-redux";
import { actLogin } from "../redux/actions/LoginAction";
import { useHistory, withRouter } from 'react-router-dom';
import Swal from 'sweetalert2'

function AuthForm ({authState, onChangeAuthState}) {

  const [isSignup, setIsSignup] = useState(false);
  const history = useHistory();

  useEffect(() => {
    if (authState === STATE_SIGNUP) {
      setIsSignup(true);
    }
  }, [])


  const changeAuthState = authState => event => {
    event.preventDefault();
    console.log(authState);
    onChangeAuthState(authState);
  };

  const dispatch = useDispatch();
  const [name, setName] = useState('');
  const [email, setEmail] = useState('');
  const [nohp, setNohp] = useState('');
  const [password, setPassword] = useState('');
  const [repassword, setRepassword] = useState('');

  const handleSubmit = async (event) => {
    event.preventDefault();
    

    if (isSignup === true) {
      
      if (password !== repassword) {
        ErrorAlert("Password Tidak Sesuai");
        return false;
      }
      if (repassword === '') {
        ErrorAlert("Ketik Ulang Password");
        return false;
      }
      if (password === '') {
        ErrorAlert("Password Harus Diisi");
        return false;
      }
      if (password.length < 8) {
        ErrorAlert("Password Minimal 8 Karakter");
        return false;
      }
      if (nohp === '') {
        ErrorAlert("No. Handphone Harus Diisi");
        return false;
      }
      if (email === '') {
        ErrorAlert("Email Harus Diisi");
        return false;
      }
      if (name === '') {
        ErrorAlert("Nama Harus Diisi");
        return false;
      }

      const data = {
        pic_name: name,
        email : email,
        no_hp : nohp,
        password : password,
        confirm_password : repassword
      };

      await axios.post(API_URL + 'api/v1/user/register', data)
      .then((response) => {
        // console.log(response)

        if(response.data.status === 201) {
          Swal.fire({
            icon: 'success',
            title: 'Register Berhasil',
            showConfirmButton: false,
            timer: 1000
          })
          .then( () => {
            history.push('/login')
          });          
        } else if (response.data.status === 204) {
          Swal.fire({
            icon: 'error',
            title: 'Error',
            text: response.data.message
          });
        }
        
      })
      .catch(function (error) {
        console.log(error);
      });

    } else {
     
      if (password === '') {
        ErrorAlert("Password Tidak Sesuai");
        return false;
      }
      if (email === '') {
        ErrorAlert("Email Harus Diisi");
        return false;
      }

      const data = {
        email : email,
        password : password
      };

      await axios.post(API_URL + 'api/v1/user', data)
      .then((response) => {
        // console.log(response)

        if(response.data.status === 200) {
          const data = {
            pic_name  : response.data.pic_name,
            email     : response.data.email,
            user_id   : response.data.user_id,
            role_id   : response.data.role_id,
            token     : response.data.token
          }
          dispatch(actLogin(data));
          Swal.fire({
            icon: 'success',
            title: 'Login Berhasil',
            showConfirmButton: false,
            timer: 1000
          })
          .then( () => {
            history.push('/')
          });          
        } else if (response.data.status === 204) {
          Swal.fire({
            icon: 'error',
            title: 'Error',
            text: response.data.message
          });
        }
        
      })
      .catch(function (error) {
        console.log(error);
      });
      
    }    

  };

  const ErrorAlert = (string) => {
      Swal.fire({
          icon: 'error',
          title: 'Oops',
          text: string,
      });
  };

  const renderButtonText = () => {
    const { buttonText } = AuthForm.defaultProps;

    if (!buttonText && isSignup) {
      return 'Daftar';
    } else {
      return 'Masuk';
    }

  }

  const {
    showLogo,
    nameLabel,
    nameInputProps,
    emailLabel,
    emailInputProps,
    nohpLabel,
    nohpInputProps,
    passwordLabel,
    passwordInputProps,
    confirmPasswordLabel,
    confirmPasswordInputProps,
    children,
    onLogoClick,
  } = AuthForm.defaultProps;

    return (
      <Form onSubmit={handleSubmit} className="p-4">
        {showLogo && (
          <div className="text-center pb-4">
            <img
              src={logo200Image}
              className="rounded"
              style={{ width: 60, height: 60, cursor: 'pointer' }}
              alt="logo"
              onClick={onLogoClick}
            />
          </div>
        )}

        {isSignup && ( 
          <div className="row my-4">
            <div className='col-4'>
              <Label className="font-weight-bold" for={nameLabel}>{nameLabel}</Label>
            </div>
            <div className='col-8'>
              <Input {...nameInputProps } onChange={(e) => setName(e.target.value)} />
            </div>
          </div>
        )}

        <div className="row my-4">
          <div className='col-4'>
            <Label className="font-weight-bold" for={emailLabel}>{emailLabel}</Label>
          </div>
          <div className='col-8'>
            <Input {...emailInputProps} onChange={(e) => setEmail(e.target.value)} />
          </div>
        </div>
        
        {isSignup && (
          <div className="row my-4">
            <div className='col-4'>
              <Label className="font-weight-bold" for={nohpLabel}>{nohpLabel}</Label>
            </div>
            <div className='col-8'>
              <Input {...nohpInputProps } onChange={(e) => setNohp(e.target.value)} />
            </div>
          </div>
        )}
        
        <div className="row my-4">
          <div className='col-4'>
            <Label className="font-weight-bold" for={passwordLabel}>{passwordLabel}</Label>
          </div>
          <div className='col-8'>
            <Input {...passwordInputProps} onChange={(e) => setPassword(e.target.value)} />
          </div>
        </div>
        
        {isSignup && (
        <>
          <div className="row my-4">
            <div className='col-4'>
              <Label className="font-weight-bold" for={confirmPasswordLabel}>{confirmPasswordLabel}</Label>
            </div>
            <div className='col-8'>
              <Input {...confirmPasswordInputProps}  onChange={(e) => setRepassword(e.target.value)} />
            </div>
          </div>
          
          <FormGroup check>
            <Label check>
              <Input type="checkbox" />{' '}
              Dengan mendaftar, saya menyetujui syarat dan ketentuan yang berlaku
            </Label>
          </FormGroup>
        </>
        )}


        <Button
          size="lg"
          className="border-0 mt-5"
          block
          color="primary"
          onClick={handleSubmit}>
          {renderButtonText()}
        </Button>

        <div className="text-center pt-4">
          <h6>
            {isSignup ? (
            <>
              <span>Sudah punya akun?</span>
              <a href="#login" onClick={changeAuthState(STATE_LOGIN)}>  Masuk</a>
            </>
            ) : (
            <>
              <span>Belum punya akun?</span>
              <a href="#signup" onClick={changeAuthState(STATE_SIGNUP)}> Daftar</a>
            </>              
            )}
          </h6>
        </div>

        {children}
      </Form>
    );
    
}

export const STATE_LOGIN = 'LOGIN';
export const STATE_SIGNUP = 'SIGNUP';

AuthForm.propTypes = {
  authState: PropTypes.oneOf([STATE_LOGIN, STATE_SIGNUP]).isRequired,
  showLogo: PropTypes.bool,
  nameLabel: PropTypes.string,
  nameInputProps: PropTypes.object,
  emailLabel: PropTypes.string,
  emailInputProps: PropTypes.object,
  nohpLabel: PropTypes.string,
  nohpInputProps: PropTypes.object,
  passwordLabel: PropTypes.string,
  passwordInputProps: PropTypes.object,
  confirmPasswordLabel: PropTypes.string,
  confirmPasswordInputProps: PropTypes.object,
  onLogoClick: PropTypes.func,
};

AuthForm.defaultProps = {
  authState: 'LOGIN',
  showLogo: false,
  nameLabel: 'Nama Lengkap',
  nameInputProps: {
    type: 'text',
    placeholder: 'Masukkan Nama Lengkap'
  },
  emailLabel: 'Email',
  emailInputProps: {
    type: 'email',
    placeholder: 'Masukkan Email',
  },
  nohpLabel: 'No. Handphone',
  nohpInputProps: {
    type: 'text',
    placeholder: 'Masukkan No. Handphone'
  },
  passwordLabel: 'Password',
  passwordInputProps: {
    type: 'password',
    placeholder: 'Masukkan Password',
  },
  confirmPasswordLabel: 'Ketik Ulang Password',
  confirmPasswordInputProps: {
    type: 'password',
    placeholder: 'Ketik Ulang Password',
  },
  onLogoClick: () => {},
};

export default withRouter(AuthForm);
