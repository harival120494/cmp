import {ACT_LOGIN} from '../actions/LoginAction';

const initialState = {
    pic_name : '',
    email    : '',
    user_id  : '',
    role_id  : '',
    token    : ''
}


function LoginReducer (state = initialState, action){
    switch (action.type) {
        case ACT_LOGIN:
            return {
                ...state, ...action.payload
            }
        default:
            return state;
    }
}

export default LoginReducer;