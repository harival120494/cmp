import React from 'react';
import ReactDOM from 'react-dom';
import { Provider } from "react-redux";
import { persistStore } from "redux-persist";
import { PersistGate } from "redux-persist/integration/react";
import Store from './redux/store/index'
import App from './App';

const persistor = persistStore(Store);

ReactDOM.render(
    <Provider store={Store}>
        <PersistGate loading={null} persistor={persistor}>
            <App />
        </PersistGate>
    </Provider>, 
    document.getElementById('root')
);
