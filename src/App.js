import { STATE_LOGIN, STATE_SIGNUP } from 'components/AuthForm';
import GAListener from 'components/GAListener';
import { EmptyLayout, LayoutRoute, MainLayout } from 'components/Layout';
import PageSpinner from 'components/PageSpinner';
import AuthPage from 'pages/AuthPage';
import React from 'react';
import componentQueries from 'react-component-queries';
import { BrowserRouter, Redirect, Route, Switch } from 'react-router-dom';
import './styles/reduction.scss';

const Marketplace = React.lazy(() => import('pages/marketplace'));
const Package = React.lazy(() => import('pages/package'));
const Periode = React.lazy(() => import('pages/periode'));
const Bank = React.lazy(() => import('pages/bank'));
const Role = React.lazy(() => import('pages/role'));
const User = React.lazy(() => import('pages/user'));

const AlertPage = React.lazy(() => import('pages/AlertPage'));
const AuthModalPage = React.lazy(() => import('pages/AuthModalPage'));
const BadgePage = React.lazy(() => import('pages/BadgePage'));
const ButtonGroupPage = React.lazy(() => import('pages/ButtonGroupPage'));
const ButtonPage = React.lazy(() => import('pages/ButtonPage'));
const CardPage = React.lazy(() => import('pages/CardPage'));
const ChartPage = React.lazy(() => import('pages/ChartPage'));
const DashboardPage = React.lazy(() => import('pages/DashboardPage'));
const DropdownPage = React.lazy(() => import('pages/DropdownPage'));
const FormPage = React.lazy(() => import('pages/FormPage'));
const InputGroupPage = React.lazy(() => import('pages/InputGroupPage'));
const ModalPage = React.lazy(() => import('pages/ModalPage'));
const ProgressPage = React.lazy(() => import('pages/ProgressPage'));
const TablePage = React.lazy(() => import('pages/TablePage'));
const TypographyPage = React.lazy(() => import('pages/TypographyPage'));
const WidgetPage = React.lazy(() => import('pages/WidgetPage'));

const getBasename = () => {
  return `/${process.env.PUBLIC_URL.split('/').pop()}`;
};

class App extends React.Component {
  render() {
    return (
      <BrowserRouter basename={getBasename()}>
        <GAListener>
          <Switch>
            <LayoutRoute
              exact
              path="/login"
              layout={EmptyLayout}
              component={props => (
                <AuthPage {...props} authState={STATE_LOGIN} />
              )}
            />
            <LayoutRoute
              exact
              path="/signup"
              layout={EmptyLayout}
              component={props => (
                <AuthPage {...props} authState={STATE_SIGNUP} />
              )}
            />
            
            <MainLayout breakpoint={this.props.breakpoint}>
              <React.Suspense fallback={<PageSpinner />}>
                <Route exact path="/" component={props => <DashboardPage {...props} />} />
                <Route exact path="/marketplace" component={props => <Marketplace {...props} />} />
                <Route exact path="/package" component={props => <Package {...props} />} />
                <Route exact path="/periode" component={props => <Periode {...props} />} />
                <Route exact path="/bank" component={props => <Bank {...props} />} />
                <Route exact path="/role" component={props => <Role {...props} />} />
                <Route exact path="/user" component={props => <User {...props} />} />

                
                <Route exact path="/userlist" component={props => <Package {...props} />} />

                <Route exact path="/login-modal" component={props => <AuthModalPage {...props} />} />
                <Route exact path="/buttons" component={props => <ButtonPage {...props} />} />
                <Route exact path="/cards" component={props => <CardPage {...props} />} />
                <Route exact path="/widgets" component={props => <WidgetPage {...props} />} />
                <Route exact path="/typography" component={props => <TypographyPage {...props} />} />
                <Route exact path="/alerts" component={props => <AlertPage {...props} />} />
                <Route exact path="/tables" component={props => <TablePage {...props} />} />
                <Route exact path="/badges" component={props => <BadgePage {...props} />} />
                <Route
                  exact
                  path="/button-groups"
                  component={props => <ButtonGroupPage {...props} />}
                />
                <Route exact path="/dropdowns" component={props => <DropdownPage {...props} />} />
                <Route exact path="/progress" component={props => <ProgressPage {...props} />} />
                <Route exact path="/modals" component={props => <ModalPage {...props} />} />
                <Route exact path="/forms" component={props => <FormPage {...props} />} />
                <Route exact path="/input-groups" component={props => <InputGroupPage {...props} />} />
                <Route exact path="/charts" component={props => <ChartPage {...props} />} />
              </React.Suspense>
            </MainLayout>
            <Redirect to="/" />
          </Switch>
        </GAListener>
      </BrowserRouter>
    );
  }
}

const query = ({ width }) => {
  if (width < 575) {
    return { breakpoint: 'xs' };
  }

  if (576 < width && width < 767) {
    return { breakpoint: 'sm' };
  }

  if (768 < width && width < 991) {
    return { breakpoint: 'md' };
  }

  if (992 < width && width < 1199) {
    return { breakpoint: 'lg' };
  }

  if (width > 1200) {
    return { breakpoint: 'xl' };
  }

  return { breakpoint: 'xs' };
};

export default componentQueries(query)(App);
